
Originally in [README.md](../README.md), _DNS_

# DNS

In order to support Kerberos, we need to configure an DNS server which will serve local domains.

Note that for small setups it's doable to simply distribute the hostname in `/etc/hosts` (`files` should be the first entry `/etc/nsswitch.conf`, `hosts` section)
https://stackoverflow.com/questions/13356274/can-etc-hosts-config-reverse-resolution

I've used BIND9, but the same can be achieved with `dnsmasq` and an `/etc/hosts` file - it would probably be a better solution for small networks, and also provides dynamic hostname resolution if you use its DHCP server.
- https://www.linux.com/topic/networking/advanced-dnsmasq-tips-and-tricks/
- https://wiki.debian.org/dnsmasq?action=show&redirect=HowTo%2Fdnsmasq
- https://wiki.archlinux.org/title/Dnsmasq

It needs to forward other requests - an authoritative name server will reply with REFUSED but since the client successfully contacted it, it won't query the next server.

Various materials used to get to this configuration:
- https://help.ubuntu.com/community/BIND9ServerHowto
- https://www.digitalocean.com/community/tutorials/how-to-configure-bind-as-an-authoritative-only-dns-server-on-ubuntu-14-04
- https://www.digitalocean.com/community/tutorials/how-to-configure-bind-as-a-private-network-dns-server-on-ubuntu-18-04
- https://www.linuxbabe.com/ubuntu/set-up-local-dns-resolver-ubuntu-20-04-bind9
- http://www.zytrax.com/books/dns/ch7/address_match_list.html
- https://kb.isc.org/docs/aa-00503


> **Do note** that while technically valid (and supported by both the `bind` DNS server and `dig` tool), `systemd-resolved` DNS stub doesn't propery handle custom TLD-only domain names (defaulting to LLMNR and other link-local name resolution protocols instead of actually querying the DNS), timing out and returning `SERVFAIL`.
>
> This can be worked around (the easiest way being completely bypassing `systemd-resolved`) but for your own sanities sake, use a multi-level FQDN - the TLD can be arbitrary (eg. `arhiv` will give you headaches, `arhiv.pecar` is ok).

```sh
apt-get install bind9 bind9utils

# Configure for IPv4 only (limitation of our network, keeping IPv6 causes issues)
echo 'OPTIONS="-4 -u bind"' > /etc/default/named
# Check with `systemctl cat named`

# Configure options and zones
cat > /etc/bind/named.conf.options <<EOF
options {
        directory "/var/cache/bind";

        allow-transfer {
          # We do not run backup DNS server, thus no need for zone transfers
          none;
        };

        # Accept queries from localhost + our subnet only
        listen-on         { localhost; localnets; };
        allow-recursion   { localhost; localnets; };
        allow-query       { localhost; localnets; };
        allow-query-cache { localhost; localnets; };

        # Will perform recursive queries
        recursion yes;

        forwarders {
          # - our gateway does not provide a nameserver
          # - we're using [Arnes](https://www.arnes.si/pomoc-uporabnikom/arnesovi-strezniki/) nameserver.

          193.2.1.66 ;
          193.2.1.72 ;
        };

        dnssec-validation auto; # Validate DNS responses (when requested) with BIND's internal keys
        auth-nxdomain no;       # Conform to RFC1035 (don't lie about caching)
        max-cache-size 10%;     # Limit memory usage - not a dedicated DNS server

        # No IPv6 resolution (not set up on our network)
        # listen-on-v6 { any; };
};
EOF

# Set up zones that resolve the Kerberos server (same host)
cat > /etc/bind/named.conf.local <<EOF
// forward zone
zone "arhiv.pecar" {
  type master;
  file "/etc/bind/zones/db.arhiv.pecar";
};

// reverse zone for 192.168.0.0/16
zone "168.192.in-addr.arpa" {
  type master;
  file "/etc/bind/zones/db.192.168";
};
EOF

# Set up zone definitions
mkdir /etc/bind/zones

# Forward zone
cat > /etc/bind/zones/db.arhiv.pecar <<'EOF'
$TTL  604800
@       IN      SOA     arhiv.pecar.  root.arhiv.pecar. (
  2021062100    ; Serial YYYYMMDDnn
      604800    ; Refresh
       86400    ; Retry
     2419200    ; Expire
      604800 )  ; Negative Cache TTL
;
                IN      NS            arhiv.pecar.
arhiv.pecar.    IN      A             192.168.100.100 ; Address of the NS
EOF

# Reverse zone
cat > /etc/bind/zones/db.192.168 <<'EOF'
$TTL  604800
@       IN      SOA     arhiv.pecar.  root.arhiv.pecar. (
  2021062100    ; Serial
      604800    ; Refresh
       86400    ; Retry
     2419200    ; Expire
      604800 )  ; Negative Cache TTL
;
        IN      NS      arhiv.pecar.
100.100 IN      PTR     arhiv.pecar.  ; Address of the NS (subnet portion)
EOF

# Check configuration
named-checkconf  # no output == OK

named-checkzone arhiv.pecar /etc/bind/zones/db.arhiv.pecar
named-checkzone 168.192.in-addr.arpa /etc/bind/zones/db.192.168

# If all checks out, start nameserver
systemctl enable --now named
```

Configure `systemd-resolved` to local DNS server
https://askubuntu.com/questions/1153516/resolvconf-yes-in-etc-default-bind9

```sh
# resolvconf, altough legacy, is required by named-resolvconf
# (resolvectl in compatibility mode fails)
apt install resolvconf
systemctl enable --now named-resolvconf
```
`/etc/resolv.conf` will still list systemd's stub (127.0.0.53). The systemd resolver is only used by local applications (in `/etc/nsswtich.conf` via the `dns` entry), remote clients querying the `named` DNS server will skip it.

The configuration can be considered functional _only_ after `resolvectl query` manages to resolve the domain.

If you were using DHCP, you might need to explicitly disable DHCP DNS assignment.
```sh
nmcli con mod 'Wired connection X' ipv4.ignore-auto-dns yes
```
Client needs to set up `ipv4.dns`, server should have no explicit configuration (should have `named-resolvconf` enabled)

Any additional modifications requires you to flush DNS caches on all clients
```sh
systemd-resolve --flush-caches
```

> For dnsmaq you might need to disable systemd's DNS stub
> - https://unix.stackexchange.com/questions/304050/> how-to-avoid-conflicts-between-dnsmasq-and-systemd-resolved
> - https://wiki.archlinux.org/title/Systemd-resolved

```sh
systemctl restart systemd-resolved

# Reconfigure network (you will need to reconnect via SSH)
# intentionally skipping DNS here, will default to local DNS
# (one can still override it by explicitly providing ipv4.dns)
nmcli con mod 'Wired connection 1' ipv4.address 192.168.100.100/16 &&
nmcli con mod 'Wired connection 1' ipv4.gateway 192.168.0.1 &&
nmcli con mod 'Wired connection 1' ipv4.dns 193.2.1.66,193.2.1.72 &&
nmcli con mod 'Wired connection 1' ipv4.method manual &&
nmcli con up 'Wired connection 1'

# you also have `nmtui` for interactive setup
```

Check that [hostname resolution works locally](https://unix.stackexchange.com/questions/186859/understand-hostname-and-etc-hosts)
```sh
# both commands should return your hostname
hostname --all-fqdn
dnsdomainname
```

On your client
```sh
# test forward and reverse resolution (bypassing systemd-resolved)
dig @192.168.100.100 arhiv.pecar          # should return 192.168.100.100
dig @192.168.100.100 -x 192.168.100.100   # should return arhiv.

# test if systemd-resolved is properly configured
resolvectl query arhiv.pecar
resolvectl query 192.168.100.100
# should also work
systemd-resolve arhiv.pecar
# should also work
getent hosts arhiv.pecar

# test recursive queries
dig @192.168.100.100 www.google.com       # result varies, you should get a valid IP, response status should be NOERROR
```

Configure your client to first query our DNS, and if that isn't available, it's forward DNSes (Arnes).

I've configured it in Plasma network manager, IPv4, Method Automatic (Only addresses), DNS servers set to `192.168.100.100,193.2.1.66,193.2.1.72`. Restart the network profile.

After this you should be able to dig / ping `arhiv` without explicitly specifying the nameserver.

_Further reading:_
 - https://www.digitalocean.com/community/tutorials/how-to-configure-bind-as-an-authoritative-only-dns-server-on-ubuntu-14-04
