## Big TODOs

There are some issues in this document that you should be aware of, since they affect system stability / security:
- **the Kerberos section is outdated** - it improperly states that service principals should reside in user keytabs, which is a big no-no.
  The final, correct, documentation is at https://unix.stackexchange.com/questions/654549/nfsv4-wrong-effective-user-owner-sec-krb5-mount-squashes-to-anonymous-user

  it is however a subsection of the documentation presented here. Read both, but be aware of the inconsistency.

<!-- omit in toc -->
# Purpose of this document

This deals with setting up and maintaining a custom NAS / backup system.
My main goals here were:
- a (software) stable linux system with reasonable LTS
- simple and effective management, given the current experience - I come from Arch Linux, so I want to retain similar tools / configurations
- lightweight environment, but not at the expense of usability - I still want to have a GUI for previewing / managing files, easy transfers / backups. Call me lazy, but you have to admit that it feels nice to just drag & drop something instead of going through various hoops in order to achieve the same through the command line.
- while customized, it should still be intuitive enough for anyone else to be able to log in and do basic stuff

The important assumption here is that **the system is only ever used at home and within a LAN**.

It's intention is to be easy to use in-person, while still maintaining reasonable network security policies.

<!-- omit in toc -->
# Table of Contents
- [Timeline](#timeline)
- [0. Initial system checkup and planning](#0-initial-system-checkup-and-planning)
  - [0.0. Get a decent understanding of ZFS](#00-get-a-decent-understanding-of-zfs)
  - [0.1. Hardware](#01-hardware)
    - [Drives](#drives)
  - [0.2. Do a DESTRUCTIVE thorough test on data store harddrives](#02-do-a-destructive-thorough-test-on-data-store-harddrives)
  - [0.3. Linux distro selection](#03-linux-distro-selection)
- [1. System Installation](#1-system-installation)
  - [1.1 Set up SSH to the server](#11-set-up-ssh-to-the-server)
  - [1.2 Format boot drive](#12-format-boot-drive)
  - [1.3 Install the system](#13-install-the-system)
  - [1.4 Set up bootloader](#14-set-up-bootloader)
  - [1.5 Clean up and boot into the system](#15-clean-up-and-boot-into-the-system)
  - [1.6. Set up the desktop environment](#16-set-up-the-desktop-environment)
  - [1.7. Set up users](#17-set-up-users)
  - [1.8. Set up i3-KDE DE](#18-set-up-i3-kde-de)
  - [1.8. Install userland tools](#18-install-userland-tools)
- [2. Set up data store](#2-set-up-data-store)
  - [2.1. Plan & test the final configuration](#21-plan--test-the-final-configuration)
  - [2.2. Set up ZFS on actual disks](#22-set-up-zfs-on-actual-disks)
- [X. LUKS encrypted data migration](#x-luks-encrypted-data-migration)
  - [Set-up and test LUKS on the system](#set-up-and-test-luks-on-the-system)
  - [Write protecting block devices](#write-protecting-block-devices)
  - [Backing up LUKS headers](#backing-up-luks-headers)
  - [Decrypting disks with header backups](#decrypting-disks-with-header-backups)
  - [Migrating important data first](#migrating-important-data-first)
- [Y. Servers](#y-servers)
  - [Network discovery](#network-discovery)
  - [Name resolution](#name-resolution)
  - [Kerberos](#kerberos)
    - [A primer on Kerberos user mapping, owners and permissions](#a-primer-on-kerberos-user-mapping-owners-and-permissions)
    - [Service principals](#service-principals)
    - [Non-root user tickets](#non-root-user-tickets)
    - [And now to actually configuring it](#and-now-to-actually-configuring-it)
    - [On principals and user mapping](#on-principals-and-user-mapping)
    - [Kerberos troubleshooting](#kerberos-troubleshooting)
    - [ZFS exports](#zfs-exports)
  - [NFS](#nfs)
  - [WebDAV](#webdav)
  - [Backups](#backups)

# Timeline
- `04/11/2020` disks ordered @ [Mlacom](https://www.mlacom.si/hitachi-hus728t8tale6l4-8tb-7-2/256m/s600-raid-edition)
- `06/11/2020` prototype system configuration started
- `13/11/2020` disks received
- `21/11/2020` disks tested
- `22/05/2021` project resumed
- `23/05/2021` basic system set up
- `02/06/2021` reworked ZFS documentation

# 0. Initial system checkup and planning

## 0.0. Get a decent understanding of ZFS

See [README_ZFS.md](README_ZFS.md)

## 0.1. Hardware

We're recycling a HP ProDesk 600 G1 SFF (Small Form Factor) here. Configured with i3-4150 @ 3.5GHz and 16GB 1600MHz memory, it's more than enough for what we need.

The service manual is available at \
http://h10032.www1.hp.com/ctg/Manual/c04331099 \
make sure you're looking at the SFF section.

It uses a custom power supply
- 702308-002 (in the machine)

Compatibles:
- 702457-001
- 702456-001
- 702455-001

so make sure you get a spare.

### Drives

---

_**Note** At the time I started the project, I've opted for RAID 1 (mirroring), where I've selected drives of above-average capacity and long warranty._

This was significantly more expensive and less space efficient then if I took the cheapest drives of lower capacity and put them in a RAIDZ.

However it makes for a simple setup, hardware-wise, and that was a primary concern for me at the time.

[This article](https://jrs-s.net/2015/02/06/zfs-you-should-use-mirror-vdevs-not-raidz/) was also pretty influential - it's slightly opinionated but states some facts that eventually reassured me that a mirror topology was a good choice for an entry-level system.

---

Drives used:
- 2x **HITACHI HUS728T8TALE6L4 8TB 7.2/256M/S600 Raid Edition** which are data store drives in ZFS mirrored vdev configuration
- 1x **Samsung 860 EVO SSD 250GB 2,5 sata3** which is the system drive

Final data store drive selection was done based on: \
  https://geizhals.at/?cat=hde7s&xf=1080_SATA+6Gb%2Fs%7E3317_2018%7E3772_3.5%7E8457_Conventional+Magnetic+Recording+(CMR)%7E8457_non-SMR%7E958_8000

Took a [CMR aka PMR](https://www.synology.com/en-global/knowledgebase/DSM/tutorial/Storage/PMR_SMR_hard_disk_drives) drive that had the longest warranty in its price range. For 8TB, the typical cost at the time of writing (Nov. 2020, Europe) seems to be around 200 EUR + tax per drive.

For the SSD I just took something reliable that the distributor had.

_References:_
- https://www.reddit.com/r/DataHoarder/wiki/hardware#wiki_hard_drives
- https://raid.wiki.kernel.org/index.php/Timeout_Mismatch
- https://nascompares.com/


---

_**May 20201**: Welp, seems that Chia + the global supply chain shortage hit the market hard. If you haven't bought your drives yet, I'd suggest that you wait for the craziness to settle down a bit. See you in 2022 ...?_

---

## 0.2. Do a DESTRUCTIVE thorough test on data store harddrives

Check that the hard drives are connected and get detected by the system.

For an initial conveyance test, do a badblocks with random+zero pattern
```
# WARNING: THIS DESTROYS ALL DATA ON TARGET-DRIVE
# badblocks -t random -t 0 -w -v -s -b 4096 /dev/disk/by-id/TARGET-DRIVE
```

In my case, testing both drives in parallel took ~46 hours (~12 hours per pass), with badblocks producing the following output for each drive
```
Checking for bad blocks in read-write mode
From block 0 to 1953506645
Testing with random pattern: done                                                 
Reading and comparing: done                                                 
Testing with pattern 0x00: done                                                 
Reading and comparing: done                                                 
Pass completed, 0 bad blocks found. (0/0/0 errors)
```

## 0.3. Linux distro selection

Since some time passed after the initial project, I decided to do a quick check of the current state of the ZFS On Linux landscape.

The following has been investigated in more detail
- [TrueNAS SCALE](https://www.truenas.com/truenas-scale/) - Interesting from an end user perspective and would befinitely be my go-to recommendation for a novice building a full featured NAS / server in the future. However, currently it's still in alpha stage, and the de-facto lockdown of the system (for stability reasons) makes it a no-go for me.
- [Custom Debian + ZFS](https://wiki.debian.org/ZFS) Debian seemed to improve it's documentation. OpenZFS project also provides a [comprehensive guide](https://openzfs.github.io/openzfs-docs/Getting%20Started/Debian/Debian%20Buster%20Root%20on%20ZFS.html). Was what I had previously decided to use, but the distro moves too slowly for my tastes. ZFS is still not completely integrated, and they [provide really old versions](https://packages.debian.org/source/buster/zfs-linux).
- [Custom Ubuntu Server + ZFS](https://openzfs.github.io/openzfs-docs/Getting%20Started/Ubuntu/Ubuntu%2020.04%20Root%20on%20ZFS.html). Their official documentation is trash, but OpenZFS has them covered. ZFS has also been provided as part of the Ubuntu distribution for some time now, so I assume that they are pretty confident about their integration / stability. They also [move faster](https://packages.ubuntu.com/source/focal/zfs-linux) + their LTS gives us 5 years of support.
  
  Also, a major bonus for using Ubuntu that I have discovered later is that they provide a zsys / zsysctl system snapshot tool, which makes system snapshot management a breeze.

I've ended up deciding for Ubuntu 20.04.2 LTS (Focal Fossa), due to the considerations mentioned above.

The original guide was written for Debian, portions that were specific to it we're moved to [install_debian.md](./install_debian.md)

# 1. System Installation

We're installing Ubuntu Server 20.04.2 (Focal Fossa) -  `ubuntu-20.04.2-live-server-amd64.iso` - [download location](https://releases.ubuntu.com/20.04/)

I'm loosely following the [OpenZFS 
Ubuntu 20.04 Root on ZFS](https://openzfs.github.io/openzfs-docs/Getting%20Started/Ubuntu/Ubuntu%2020.04%20Root%20on%20ZFS.html#step-1-prepare-the-install-environment) guide,
making changes / skipping / shuffling steps as I go along.

Effort has been made to document the exact actions taken, so by following the guide you should end up with the same system.

---

**Note that I'm configuring the system with localizations for Slovenia**.

Be sure to change repository, timezone, etc. configurations if you're following along from some other country.


---

**Note about copy-pasting**: some commands (such as `apt`) gobble up additional terminal input, so copy-pasting whole blocks might actually skip some commands.

Someday I'll rework this into a script, but until then, copy each line separately and check it's output.

---

## 1.1 Set up SSH to the server

_so that you lazy people can follow the guide + configure the server on your local system_

Boot the server with the install image (baked on USB).

When the installer starts, set up keyboard, network (set up static IP, preferably at final IP address of the server), make note of the suggested repo (we will have to set it up manually).
After this is configured, you can move to shell (Ctrl+Alt+F2 / F3 / ...)

Make sure that server has the right time
```sh
dpkg-reconfigure tzdata

timedatectl set-ntp true

# systemd-timesyncd waits for network configuration change to start sync
ip link set eno1 down # eno1 is network adapter name, may vary
ip link set eno1 up   # will keep previous IP configuration
```

Configure for root access (applicable only to this live-cd session)
```sh
sudo -i
echo 'PermitRootLogin yes' > /etc/ssh/sshd_config.d/permit_root_login.conf
passwd # for root
```

Open session on your local system
```sh
ssh root@192.168.whatever
```

## 1.2 Format boot drive

Install zfs utilities
```sh
apt update
apt install --yes debootstrap gdisk zfs-initramfs
```

Set up environment variables for your drives (I probably don't need to stress that everything from this point onwards is destructive). It _should_ leave your data drives intact however.
```sh
BOOTDISK=/dev/disk/by-id/ata-Samsung_SSD_860_EVO_250GB_whatever
```

Note that if there are any valid pools (from previous attempts), one needs to import + destroy them first, otherwise pool creation will fail (and while forcing will create new pool, it won't automount it)
```sh
zpool import # should report 'no pools availablt to import', otherwise
# zpool destroy [pool name]
```

Check if any partition information already present & wipe (add `-a` option)
```sh
wipefs $BOOTDISK
```

Set up bootdisk partitions. I've loosely followed the guide - this depends on your disk, I've did this interactively with `gdisk`, the end result was
```
root@ubuntu-server:~# sgdisk -p $BOOTDISK
Disk /dev/disk/by-id/ata-Samsung_SSD_860_EVO_250GB_S3YJNMFN910204N: 488397168 sectors, 232.9 GiB
Sector size (logical/physical): 512/512 bytes
Disk identifier (GUID): 9BB4AFAB-9F9A-4BE3-8F07-A827C20C4D4C
Partition table holds up to 128 entries
Main partition table begins at sector 2 and ends at sector 33
First usable sector is 34, last usable sector is 488397134
Partitions will be aligned on 2048-sector boundaries
Total free space is 2014 sectors (1007.0 KiB)

Number  Start (sector)    End (sector)  Size       Code  Name
   1            2048         1050623   512.0 MiB   EF00  EFI system partition
   2         1050624         9439231   4.0 GiB     BE00  Solaris boot
   3         9439232       488397134   228.4 GiB   BF00  Solaris root
```

I've decided not to use a swapfile - it's not a desktop, so it will never be put into hibernation, and its I/O throughput would probably make me weep.

Create boot pool (copied directly from guide)
```sh
zpool create \
    -o cachefile=/etc/zfs/zpool.cache \
    -o ashift=12 -o autotrim=on -d \
    -o feature@async_destroy=enabled \
    -o feature@bookmarks=enabled \
    -o feature@embedded_data=enabled \
    -o feature@empty_bpobj=enabled \
    -o feature@enabled_txg=enabled \
    -o feature@extensible_dataset=enabled \
    -o feature@filesystem_limits=enabled \
    -o feature@hole_birth=enabled \
    -o feature@large_blocks=enabled \
    -o feature@lz4_compress=enabled \
    -o feature@spacemap_histogram=enabled \
    -O acltype=posixacl -O canmount=off -O compression=lz4 \
    -O devices=off -O normalization=formD -O relatime=on -O xattr=sa \
    -O mountpoint=/boot -R /mnt \
    bpool ${BOOTDISK}-part2
```

Create root pool (copied directly from the guide, un-encrypted option)
```sh
zpool create \
    -o ashift=12 -o autotrim=on \
    -O acltype=posixacl -O canmount=off -O compression=lz4 \
    -O dnodesize=auto -O normalization=formD -O relatime=on \
    -O xattr=sa -O mountpoint=/ -R /mnt \
    rpool ${BOOTDISK}-part3
```

Configure boot, root datasets (we need the SomeName_$UUID, otherwise zsys crashes at revert).
Use this exact configuration in order to have functional integration with zsys tool!
```sh
zfs create -o canmount=off -o mountpoint=none rpool/ROOT
zfs create -o canmount=off -o mountpoint=none bpool/BOOT

UUID=$(dd if=/dev/urandom bs=1 count=100 2>/dev/null |
    tr -dc 'a-z0-9' | cut -c-6)

zfs create -o mountpoint=/ \
    -o com.ubuntu.zsys:bootfs=yes \
    -o com.ubuntu.zsys:last-used=$(date +%s) rpool/ROOT/server_$UUID

zfs create -o mountpoint=/boot bpool/BOOT/server_$UUID
```

The guide calls for creation of other datasets, but this topology is not enforced.
We will keep everything under the same root dataset (everything will be part of the same snapshot).

Create persistent home folder (with rpool properties)
```
zfs create -o canmount=on -o mountpoint=/home rpool/home
```

Create and hold the snapshot of the initial dataset, thus preventing it from being destroyed
```
root@arhiv ~ # zfs snapshot rpool/home@origin 
root@arhiv ~ # zfs hold origin rpool/home@origin

root@arhiv ~ # zfs holds rpool/home@origin 
NAME               TAG     TIMESTAMP
rpool/home@origin  origin  Tue Jun 22 15:02 2021


## destroying a dataset will now fail
# zfs destroy rpool/home_ButNotReally

## recursively destroying a dataset will also fail to delete it,
## but NOTE that any nested dataset without such protection WILL get destroyed
# zfs destroy rpool/home_ButNotReally -r
```

Make sure that pools + datasets are created and mounted
```sh
root@ubuntu-server:~# echo $UUID # specific to your instance, make note of it
rmohs7

root@ubuntu-server:~# zfs list
NAME                       USED  AVAIL     REFER  MOUNTPOINT
bpool                      672K  3.62G       96K  /mnt/boot
bpool/BOOT                 192K  3.62G       96K  none
bpool/BOOT/server_rmohs7    96K  3.62G       96K  /mnt/boot
rpool                      812K   221G       96K  /mnt
rpool/ROOT                 200K   221G       96K  none
rpool/ROOT/server_rmohs7   104K   221G      104K  /mnt
rpool/home                  96K   221G       96K  /mnt/home

root@ubuntu-server:~# mount | grep /mnt
rpool/ROOT/server_rmohs7 on /mnt type zfs (rw,relatime,xattr,posixacl)
bpool/BOOT/server_rmohs7 on /mnt/boot type zfs (rw,nodev,relatime,xattr,posixacl)
rpool/home on /mnt/home type zfs (rw,relatime,xattr,posixacl)
```

## 1.3 Install the system

Set up and configure the system
```sh
debootstrap focal /mnt

mkdir /mnt/etc/zfs
cp /etc/zfs/zpool.cache /mnt/etc/zfs/

# "arhiv" is our hostname
echo arhiv > /mnt/etc/hostname

# Skip netplan setup, we will use NetworkManager
```

We use short hostname and set our local FQDN in `/etc/hosts`
https://serverfault.com/questions/331936/setting-the-hostname-fqdn-or-short-name

Configure the package sources:
```sh
grep -v 'deb cdrom' /etc/apt/sources.list | sed 's/archive.ubuntu.com/si.archive.ubuntu.com/' > /mnt/etc/apt/sources.list
```

Bind the virtual filesystems from the LiveCD environment to the new system and chroot into it:
```sh
mount --rbind /dev  /mnt/dev
mount --rbind /proc /mnt/proc
mount --rbind /sys  /mnt/sys
chroot /mnt /usr/bin/env BOOTDISK=$BOOTDISK UUID=$UUID bash --login
```

Check that you've actually chrooted and that root is actually rpool
```sh
mount | grep ' / '
# rpool/ROOT/server_rmohs7 on / type zfs (rw,relatime,xattr,posixacl)
```

Configure a basic system environment:
```sh
apt update
dpkg-reconfigure locales tzdata keyboard-configuration console-setup
# Generate + use en_US.UTF-8, tzdata Ljubljana, Generic 105 key Slovenian keyboard, UTF-8 encoding on console, Latin2, Terminus font (8x16)
```

Set up basic utilities
```sh
apt install --yes zsh man vim mc openssh-server
chsh -s /usr/bin/zsh
wget -O /etc/zsh/zshrc https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc
wget -O /etc/skel/.zshrc  https://git.grml.org/f/grml-etc-core/etc/skel/.zshrc

```

On your **local machine** (the one you use right now for configuring the server) set up SSH keys and transfer them to server.
```sh
ssh-keygen -t ed25519
cat ~/.ssh/id_ed25519.pub | xsel

> You can generate key to non-default file and refer to it explicitly when connecting (`ssh -i ~/.ssh/id_ed25519_arhiv root@192.168.100.100`) or use an [SSH Agent](https://wiki.archlinux.org/title/SSH_keys#SSH_agents)

```
On the server
```sh
mkdir ~/.ssh
chmod 700 ~/.ssh
echo '[paste key content]' > ~/.ssh/authorized_keys
chmod 600 ~/.ssh/authorized_keys

cat > /etc/ssh/sshd_config.d/permit_root_login.conf <<EOF
PermitRootLogin yes
EOF

cat > /etc/ssh/sshd_config.d/disable_password_login.conf <<EOF
PermitEmptyPasswords no
PasswordAuthentication no
EOF
```

Set up NetworkManager (will automatically set up DHCP for connected interfaces)
```sh
apt install --yes network-manager

cat > /etc/netplan/01-network-manager-all.yaml <<EOF
network:
  version: 2
  renderer: NetworkManager
EOF
```

Configure static IP address. The values used depend on your network.
In my case:
- we're using a local IPv4 192.168.0.0/16 network pool, with
  - 0.1 reserved for gateway (outbound router)
  - 0.10-63 reserved for DHCP devices
  - 0.64-254 reserved for legacy statically allocated systems
  - the rest is currently unallocated
- our gateway does not provide a nameserver
- we're using [Arnes](https://www.arnes.si/pomoc-uporabnikom/arnesovi-strezniki/) nameserver.
  - `193.2.1.66` primary
  - `193.2.1.72` backup
  
  It's a fast and portable option for Slovenia
  - it's a magnitude faster than google's DNS `8.8.8.8`
  - in some cases it's actually faster than ISP's own nameservers

We will use [libnss-myhostname](https://manpages.debian.org/unstable/libnss-myhostname/nss-myhostname.8.en.html) for localhost / hostname resolution, that will follow the current network connection.

Note that myhostname does not support FQDNs https://github.com/systemd/systemd/issues/15894

```
# Install localhost resolver
apt install libnss-myhostname

# Configure name resolution to account for myhostname and systemd-resolved
cat > /etc/nsswitch.conf <<EOF
# /etc/nsswitch.conf

passwd:         files systemd
group:          files systemd
shadow:         files
gshadow:        files

hosts:          files myhostname resolve [!UNAVAIL=return] dns
networks:       files

protocols:      db files
services:       db files
ethers:         db files
rpc:            db files

netgroup:       nis
EOF

# Set default hosts file
#   hostname is managed by myhostname
#   FQDN needs to be specified here or managed by nameserver
#
cat > /etc/hosts <<EOF
127.0.0.1       localhost localhost.localdomain
192.168.100.100 arhiv.pecar
EOF
```

Configure tmpfs
```
cp /usr/share/systemd/tmp.mount /etc/systemd/system/
systemctl enable tmp.mount
```

## 1.4 Set up bootloader

Set up EFI partition
```sh
apt install --yes dosfstools
mkdosfs -F 32 -s 1 -n EFI ${BOOTDISK}-part1
mkdir /boot/efi
echo /dev/disk/by-uuid/$(blkid -s UUID -o value ${BOOTDISK}-part1) \
    /boot/efi vfat defaults 0 0 > /etc/fstab
mount /boot/efi

mkdir /boot/efi/grub /boot/grub
echo /boot/efi/grub /boot/grub none defaults,bind 0 0 >> /etc/fstab
mount /boot/grub

apt install --yes \
    grub-efi-amd64 grub-efi-amd64-signed linux-image-generic \
    shim-signed zfs-initramfs zsys

apt remove --purge os-prober
```

Set up GRUB
```sh
update-initramfs -c -k all

cat > /etc/default/grub <<EOF
# If you change this file, run 'update-grub' afterwards to update
# /boot/grub/grub.cfg.
# For full documentation of the options in this file, see:
#   info -f grub -n 'Simple configuration'

GRUB_DEFAULT=0
GRUB_TIMEOUT=3
GRUB_RECORDFAIL_TIMEOUT=3
GRUB_DISTRIBUTOR=`lsb_release -i -s 2> /dev/null || echo Debian`
GRUB_CMDLINE_LINUX_DEFAULT="init_on_alloc=0"
GRUB_CMDLINE_LINUX=""
EOF

update-grub

grub-install --target=x86_64-efi --efi-directory=/boot/efi \
    --bootloader-id=ubuntu --recheck --no-floppy

```

Fix filesystem mount ordering
```sh
mkdir /etc/zfs/zfs-list.cache
touch /etc/zfs/zfs-list.cache/bpool
touch /etc/zfs/zfs-list.cache/rpool
ln -s /usr/lib/zfs-linux/zed.d/history_event-zfs-list-cacher.sh /etc/zfs/zed.d
zed -F &


# Verify that zed updated the cache by making sure these are not empty:

cat /etc/zfs/zfs-list.cache/bpool
cat /etc/zfs/zfs-list.cache/rpool

# If either is empty, force a cache update and check again:
#
#zfs set canmount=on bpool/BOOT/server_$UUID
#zfs set canmount=on rpool/ROOT/server_$UUID

sleep 10
# Once the files have data, stop zed:
kill %1

# Fix the paths to eliminate /mnt
sed -Ei "s|/mnt/?|/|" /etc/zfs/zfs-list.cache/*
```

## 1.5 Clean up and boot into the system

Set root password
```
passwd
```

Clean up and reboot
```sh
# Exit from chroot back to LiveCD root shell
exit

# Unmount everything
mount | grep -v zfs | tac | awk '/\/mnt/ {print $3}' | \
    xargs -i{} umount -lf {}
zpool export -a

reboot
```

If everything went well, the system boots and you should be able to log-in via the SSH key you've set up.

**On your local machine**
```sh
ssh root@192.168.0.whatever
# Should log you into 'root@arhiv' 
```

Once we're running the system, perform the first system snapshot (more on this in the next chapter)

*Note* at Ubuntu 20.04 LTS, Zsys 0.4.8 toolset doesn't properly handle version names with spaces (`zsysctl` can manage them, however menu generation fails and you're stuck with an unbootable system).

```sh
zsysctl save --system "MinimalInstall"

# Check that operation generated snapshots for all datasets
zfs list -t snapshot

# Check via the zsysctl tool that the snapshot is present
zsysctl show
```
On next boot you should have "History for Ubuntu 20.04 LTS" menu option where you can revert back to the created snapshot in case something goes awry.

You can also make additional checkpoints during the installation (the apt package management tool will probably create a couple too automatically).

## 1.6. Set up the desktop environment

Ugrade the minimal system
```sh
apt dist-upgrade --yes

# ubuntu-standard is essentially bloat, so I don't recommend it
# apt install --yes ubuntu-standard
```

Disable log compression for all currently installed applications (you might need to rerun this)
```sh
for file in /etc/logrotate.d/* ; do
    if grep -Eq "(^|[^#y])compress" "$file" ; then
        sed -i -r "s/(^|[^#y])(compress)/\1#\2/" "$file"
    fi
done
```

Reboot
```sh
reboot
```

Install the absolute minimal required for a functional KDE DE
```sh
apt install xserver-xorg-core xserver-xorg-video-intel xserver-xorg-input-evdev x11-xserver-utils x11-xkb-utils x11-utils --no-install-recommends
apt install sddm sddm-theme-breeze --no-install-recommends
apt install kde-plasma-desktop kwin systemsettings kde-config-gtk-style breeze-gtk-theme kde-config-screenlocker kscreen ksysguard plasma-nm plasma-pa kio-extras ark --no-install-recommends
```

By default SDDM uses an internal theme, which is a stripped down "Maui" theme, and it's inconsistent with the Plasma lockscreen

```sh
mkdir /etc/sddm.conf.d

cat > /etc/sddm.conf.d/theme.conf <<EOF
[Theme]
Current=breeze
EOF
```

Upstream Breeze theme currently doesn't handle passwordless accounts - patch the theme
```sh
wget https://invent.kde.org/tpecar/plasma-workspace/-/raw/156ea808fc1d1cfc4aee1784e1141d465fa1fa07/sddm-theme/Login.qml -O /usr/share/sddm/themes/breeze/Login.qml
```

Note that some core packages might get left out - if you're missing something, check the [kde-plasma-desktop metapackage](https://packages.ubuntu.com/focal/kde-plasma-desktop) and drill down to the functionality you seem to be missing.


If you want to rotate the display during login manager
```sh
cat > /usr/share/sddm/scripts/Xsetup <<EOF
#!/bin/sh
xrandr --output DP-2 --rotate left
EOF
```

## 1.7. Set up users

Create `arhiv` user which will be for normal interaction with the system


```sh
NEWUSER=arhiv

# To create a user (without creating datasets, etc.)
useradd $NEWUSER

# *note* if you _want_ to create USERDATA datasets (zsysctl managed)
# adduser $NEWUSER

cp -a /etc/skel/. /home/$NEWUSER
chown -R $NEWUSER:$NEWUSER /home/$NEWUSER
usermod -a -G adm,cdrom,dip,plugdev $NEWUSER
chsh -s /usr/bin/zsh arhiv
```

Additionally, if you want paswordless login for the user
```sh
# for PAM to allow SDDM to authenticate nopasswdlogin users without password
perl -i -n -l -e'if (/# (.*nopasswdlogin)/) {print $1} else {print}' /etc/pam.d/sddm

groupadd --system nopasswdlogin
usermod -a -G nopasswdlogin $NEWUSER

# In order for SDDM to display "press to login" instead of the password prompt
# remove the current password (will clear the password digest in /etc/shadow, but keep the 'x' in /etc/passwd)
passwd -d $NEWUSER

# manually edit /etc/passwd, remove 'x' in password field for the user in question (so that it's actually empty)
vipw
```


Further reading:
- https://github.com/sddm/sddm/issues/751

## 1.8. Set up i3-KDE DE

This is obviously a very specific preference, but once it's set up, it provides a pleasant user experience.

We still want to retain the default Kwin-Plasma DE by default though so that we don't confuse other users.

```sh
apt install i3 suckless-tools feh --no-install-recommends
```
Manually backport terminus-otb since pango stripped PCF font suppot so you can't use the original version (provided by xfonts-terminus)
```sh
# apt remove --purge xfonts-terminus # if PCF version installed
wget ftp://ftp.arnes.si/mirrors/ubuntu/pool/universe/x/xfonts-terminus/fonts-terminus-otb_4.48-3_all.deb
apt install ./fonts-terminus-otb_4.48-3_all.deb
```

Provide an i3 configuration that integrates well with Plasma
```sh
wget https://gitlab.com/tpecar/arhiv/-/raw/master/etc/i3/config -O /etc/i3/config

# Wallpaper
wget https://gitlab.com/tpecar/arhiv/-/raw/master/etc/i3/bg.png -O /etc/i3/bg.png
```

See comments in the config file, additional information available at
- https://i3wm.org/docs/userguide.html
- https://userbase.kde.org/Tutorials/Using_Other_Window_Managers_with_Plasma
- https://ryanlue.com/posts/2019-06-13-kde-i3
- https://github.com/heckelson/i3-and-kde-plasma
- https://medium.com/hacker-toolbelt/i3wm-on-debian-10-buster-c302420853b1

Provide an Xsession desktop entry that starts plasma with i3 as the window manager
```sh
cat > /usr/share/xsessions/plasma-i3.desktop <<EOF
[Desktop Entry]
Type=XSession
Exec=env KDEWM=/usr/bin/i3 /usr/bin/startplasma-x11
DesktopNames=KDE
Name=Plasma (i3)
EOF
```

Force SDDM to always preselect the more user-friendly configuration
```sh
# Remove all i3-only sessions
rm /usr/share/xsessions/i3*

# Set up order
mv /usr/share/xsessions/plasma.desktop /usr/share/xsessions/00-plasma.desktop
mv /usr/share/xsessions/plasma-i3.desktop /usr/share/xsessions/01-plasma-i3.desktop

# Disable last logged-in user / session state saving, this forces sddm to select first *.desktop file
cat > /etc/sddm.conf.d/default-session.conf <<EOF
[Users]
RememberLastUser=false
RememberLastSession=false
EOF
```

_Further reading:_
- https://wiki.ubuntu.com/CustomXSession

In order to remove the 30s spinning screen at login, go to \
_System Settings > Apperance > Workspace Theme > Splash Screen_ \
and set it to None

To get the KDE pager play nicely with i3, configure
- Spawn multiple applications on multiple pages to make the pager visible, Configure Pager with _Pager > General > Display: Desktop name_
- Make sure that _Task Manager > General > Filters > Show only tasks from current desktop_ is ticked

_References:_
- https://forum.kde.org/viewtopic.php?f=17&t=133851

## 1.8. Install userland tools

Install repo tools for file, image management and browsing
```bash
apt install p7zip-full unrar unzip zip
apt install kate okteta krename kompare krusader
apt install fonts-freefont-otf firefox gimp krita qpdfview vlc libreoffice
```

Install XnViewMP for gallery management
https://www.xnview.com/en/xnviewmp/
```bash
wget https://download.xnview.com/XnViewMP-linux-x64.deb
apt install ./XnViewMP-linux-x64.deb
```

For checking disk status install smartmontools, hdparm
```
apt install --no-install-recommends smartmontools hdparm
```

# 2. Set up data store

```
# Create pool, don't mount it's dataset (use it only as a container)
zpool create \
    -O acltype=posixacl -O canmount=off -O readonly=on -O compression=lz4 \
    -O dnodesize=auto -O normalization=formD -O relatime=on \
    -O xattr=sa -O mountpoint=/arhiv \
    \
    -n \
    arhiv-pool \
    mirror /dev/disk/by-id/wwn-0x5000cca0bece170e /dev/disk/by-id/wwn-0x5000cca0bece1673

# Create main dataset (others will be created based on backup strategies)
zfs create -o canmount=on -o readonly=off arhiv-pool/main

# Protect the dataset from dropping
zfs snapshot arhiv-pool/main@origin 
zfs hold origin arhiv-pool/main@origin

zfs holds arhiv-pool/main@origin 
```

## 2.1. Plan & test the final configuration

On virtual disks.

All operations that we anticipate to do.

Partition some data into datasets, but do that sparingly - essentially, by how you intend to do snapshots & backups.
Determine which datasets move fast and split them from the rest.

## 2.2. Set up ZFS on actual disks

Do the setup operations.

# X. LUKS encrypted data migration

One of the main reasons for creating a new backup system was to migrate my old backups from my tinfoil hat days.
Back then I had the tendency to be overly paranoid about my systems, setting up security measures without really thinking about the reasons and the principles behind them.

That lead to increased complexity with no apparent benefits, and data loss on more than one occasion.

Since such pratices seem to be quite common in our community, I suppose this section will come handy.

## Set-up and test LUKS on the system

## Write protecting block devices

Command is possibly dangerous (no anecdotal evidence that is true).
Some research has been done:
- https://superuser.com/questions/742987/why-do-i-need-to-hdparm-r-in-order-to-mount-a-sony-camcorder-read-write-and-w/1600394#1600394
- https://www.oreilly.com/library/view/linux-device-drivers/0596000081/ch12s06.html

https://www.thegeekdiary.com/how-to-disable-write-access-to-usb-devices-using-hdparm-tool/

```
# Do a test disk and expose it as a block (loopback) device

DO LOSETUP

# Query current status
hdparm -r

# Set read-only
hdparm -r1 DEVICE

# At this point, no write operation is possible (header safe) until reconnect / set rw
```

## Backing up LUKS headers

## Decrypting disks with header backups

## Migrating important data first

Copy via whitelist, but at the same time, recreate whole directory structure
Do via rsync

# Y. Servers

Setting up secure file shares has been a major source of *fun*.

TODO: set up proper principals, check whether GSSAPI is functional

## Network discovery

Is a major PITA currently
- https://en.wikipedia.org/wiki/Zero-configuration_networking#Service_discovery
- https://docs.microsoft.com/en-US/windows-server/storage/file-server/troubleshoot/smbv1-not-installed-by-default-in-windows

Until third-party WS-Discovery servers mature enough to be on critical infrastructure, 
we'll just punch in IPs like we're in the 80's again.

Reasonable people would just set up a DNS, which we do, but we complicate matters by not keeping it online.

## Name resolution

In order to support Kerberos, the client needs to be able to forward and reverse resolve the KDC and the Kerberized services.

Enterprise setup configure an DNS server which serves local domains but for small setups it's doable to simply distribute the server hostname in `/etc/hosts`.
https://stackoverflow.com/questions/13356274/can-etc-hosts-config-reverse-resolution

On your client, check / set that

- `/etc/nsswitch.conf` lists `hosts: files` (`files` should preferably be the first entry)
- `/etc/hosts` lists the address of the server (eg. `192.168.100.100 arhiv.pecar`)

Check that the system can forward / reverse resolve the name
```
resolvectl query arhiv.pecar
resolvectl query 192.168.100.100
```

## Kerberos

For authentication via NFSv4 / SMB.

I lament the fact that file sharing solutions go from broken-by-design to downright absurd, NFS being a fine example. You either have zero security, or an enterprise grade deployment.

Sigh. Have a read
- Initial installation on Debian systems http://techpubs.spinlocksolutions.com/dklar/kerberos.html
- Simplified configuration https://wiki.archlinux.org/title/Kerberos
- Key configuration (MIT Kerebros) https://wiki.archlinux.org/title/Kerberos#Without_remote_kadmin
- https://wiki.debian.org/NFS/Kerberos
- https://www.rootusers.com/how-to-use-kerberos-to-control-access-to-nfs-network-shares/

Additional docs
- https://stealthbits.com/blog/what-is-kerberos/
- https://www.tarlogic.com/en/blog/how-kerberos-works/
- Key configuration (Heimdall) http://ronny-mueller.com/2017/02/18/howto-kerberos-nfsv4-zfs-kerberized-nfs/
- https://serverfault.com/questions/947201/how-does-nfs-figure-out-which-kerberos-creds-principals-to-give-access-to
- https://tbellembois.github.io/kerberos.html

Again, in order to set up Kerberos, the both the server and client need to be able to resolve the server (zeroconf name services usually lack reverse name resolution, `/etc/hosts` is ok).

```sh
apt install krb5-admin-server krb5-kdc

# on client you can install a subset
apt install gcc libkrb5-dev krb5-user

# Set all realms to uppercase hostname (everything is hosted by this server)
# This will create krb5.conf, which you will need to rework

# Example /etc/krb5.conf
#
# arhiv.pecar is the server FQDN
# ARHIV.PECAR is the Kerberos realm
#
cat > /etc/krb5.conf <<EOF
[libdefaults]
  default_realm = ARHIV.PECAR

# The following krb5.conf variables are only for MIT Kerberos.
  kdc_timesync = 1
  ccache_type = 4
  forwardable = true
  proxiable = true

[realms]
  ARHIV.PECAR = {
    kdc = arhiv.pecar
    admin_server = arhiv.pecar
  }

[domain_realm]
  .arhiv.pecar = ARHIV.PECAR
  arhiv.pecar = ARHIV.PECAR

[logging]
  kdc          = SYSLOG:NOTICE
  admin_server = SYSLOG:NOTICE
  default      = SYSLOG:NOTICE
EOF

cat > /etc/krb5kdc/kdc.conf <<'EOF'
[kdcdefaults]
    kdc_ports = 750,88

[realms]
    ARHIV.PECAR = {
        database_name = /var/lib/krb5kdc/principal
        admin_keytab = FILE:/etc/krb5kdc/kadm5.keytab
        acl_file = /etc/krb5kdc/kadm5.acl
        key_stash_file = /etc/krb5kdc/stash
        kdc_ports = 750,88
        max_life = 10h 0m 0s
        max_renewable_life = 7d 0h 0m 0s
        master_key_type = des3-hmac-sha1
        #supported_enctypes = aes256-cts:normal aes128-cts:normal
        default_principal_flags = +preauth
    }
EOF
```

> Note that if `[realms]` section is misconfigured / missing, [Kerberos will try to find the KDC via DNS](https://web.mit.edu/kerberos/krb5-latest/doc/admin/realm_config.html), which will fail (and at the same time produce NXDOMAIN errors in `systemd-resolved`).
> 
> If you actuall want to use the DNS approach and you want to make additional diagnostics on which domains are probed via DNS, see here https://askubuntu.com/questions/1058750/new-alert-keeps-showing-up-server-returned-error-nxdomain-mitigating-potential#comment2081707_1091556

### A primer on Kerberos user mapping, owners and permissions

As noted on https://unix.stackexchange.com/questions/654549/nfsv4-wrong-effective-user-owner-sec-krb5-mount-squashes-to-anonymous-user/654632#654632

You need to understand _how_ NFS and Kerberos deal with users, and how principal names come to play here - topics that are frequently left out in most guides.

---

In kerberized NFS, one needs to be aware of the difference between
- **the effective user executing the file operation**

  The effective (server-local) user of the file operation is determined by Kerberos' [local authorization interface](https://web.mit.edu/kerberos/krb5-1.13/doc/plugindev/localauth.html), which is [configured via `auth_to_local` tag](https://web.mit.edu/kerberos/krb5-1.12/doc/admin/conf_files/krb5_conf.html#realms), and if none given, defaults to `auth_to_local = DEFAULT`, the operation for which is defined as

  > The principal name will be used as the local user name. If the principal has more than one component or is not in the default realm, this rule is not applicable and the conversion will fail.

  In the case of conversion failure it assumes the anonymous user (`nobody:nogroup` or `anonuid:anongid` if specified in exports entry)

  [Related discussion that provides a good explanation.](https://docs.pagure.org/sssd.sssd/design_pages/nss_with_kerberos_principal.html)

- **the parameters / results of this file operation**
  
  When the NFS server performs the file operation
  1. parameters need to be mapped from client-provided users to server-local users (`chown` command)
  2. results need to be mapped back to client-local users (`ls` command)
  
  This is handled by the `id_resolver` upcall program specified in `/etc/request-key.conf` or `/etc/request-key.d/*` (usually `nfsidmap`).
  
  Usernames are transferred between hosts as `user@dns_domain` strings and are mapped to local uid/gid on each side.

The effective user performing the file operation is derived from the ticket (thus it matters under which principal we authentecate), and _NOT_ from the uid of the locally running process requesting the file operation (which one might naively expect).

---

So, in order for the effective user mapping to work, one needs to create a `username` principal (effectively `username@REALM`) or, if a more complex principal name is used, provide an appropriate `auth_to_local` mapping in `/etc/krb5.conf`, as described here
- https://access.redhat.com/articles/4040141
- https://blog.samoylenko.me/2015/04/15/hadoop-security-auth_to_local-examples/
- https://community.cloudera.com/t5/Community-Articles/Auth-to-local-Rules-Syntax/ta-p/245316

---

Each user _should have its own_ default principal (`user/client-fqdn`), which is used for mapping to server-local user.

All users on single client _can share_ the service principal (`nfs/client-fqdn`).

### Service principals

As noted on [Arch Wiki](https://wiki.archlinux.org/title/Kerberos#Service_principals_and_keytabs)
> A kerberos principal has three components, formatted as `primary/instance@REALM`
> - For user principals, the primary is your username and the instance is omitted or is a role (eg. "admin"): `myuser@EXAMPLE.COM` or `myuser/admin@EXAMPLE.COM`
> - For hosts, the primary is "host" and the instance is the server FQDN: `host/myserver.example.com@EXAMPLE.COM`
> - For services, the primary is the service abbreviation and the instance is the FQDN: `nfs/myserver.example.com@EXAMPLE.COM`. The realm can often be omitted, the local computer's default realm is usually assumed. 

https://documentation.suse.com/it-it/sles/12-SP4/html/SLES-all/cha-security-kerberos.html

A deeper explanation on the principal names is available in `rpc.gssd` man page

>   rpc.gssd searches the local system's keytab for a principal and key to use to establish the machine credential.  By default, rpc.gssd assumes the file /etc/krb5.keytab contains principals and keys that can be used to obtain machine credentials.
> 
>   rpc.gssd  searches  in the following order for a principal to use.  The first matching credential is used.  For the search, <hostname> and <REALM> are replaced with the local system's hostname and
>   Kerberos realm.
> ```
>      <HOSTNAME>$@<REALM>
>      root/<hostname>@<REALM>
>      nfs/<hostname>@<REALM>
>      host/<hostname>@<REALM>
>      root/<anyname>@<REALM>
>      nfs/<anyname>@<REALM>
>      host/<anyname>@<REALM>
> ```
>   The `<anyname>` entries match on the service name and realm, but ignore the hostname.  These can be used if a principal matching the local host's name is not found.

### Non-root user tickets

Additional note here, since this is unintuitive - NFS with Kerberos will authentecate every file operation (every file / folder read, etc.), which means you need a valid ticket.

Tickets are given _per user_ so if a user doesn't have access to a valid keytab
- all operations (even `cd`) will fail as `access denied`
- operations on the mount point itself will fail with `stale file handle`

Client keytabs location is probed before the default keytab, location specified in `krb5.conf`
```
       default_client_keytab_name
              This  relation  specifies  the  name of the default keytab for obtaining client credentials.  The default is FILE:/var/kerberos/krb5/user/%{euid}/client.keytab.  This relation is subject to
              parameter expansion (see below).  New in release 1.11.

```

Further reading:
- https://access.redhat.com/articles/4040141
- https://linux-nfs.vger.kernel.narkive.com/sq5nwqXz/nfs-v4-server-127-0-0-1-does-not-accept-raw-uid-gids-reenabling-the-idmapper
- https://serverfault.com/questions/503830/regular-user-can-not-access-nfs4-mounts-permission-denied
- https://serverfault.com/questions/990397/give-linux-service-user-access-to-kerberos-nfs-share

### And now to actually configuring it

On server
```sh
apt install nfs-kernel-server nfs-common
```

Configure NFS server for krb5
```sh
vim /etc/default/nfs-common
NEED_GSSD=yes
NEED_IDMAPD=yes
```

Set up KDC ACLs (to allow administrative operations from `*/admin` principals)

```sh
cat > /etc/krb5kdc/kadm5.acl <<EOF
*/admin@ARHIV.PECAR *
EOF
```

```
# Set up database
krb5_newrealm

# Restart services
systemctl enable --now krb5-kdc.service
systemctl enable --now krb5-admin-server.service
```

Set up ID mapping on both client and server

Domain must be set to NFS server FQDN (`arhiv.pecar` in our case) on both server and client
```sh
cat > /etc/idmapd.conf <<'EOF'
[General]

Verbosity = 5
Pipefs-Directory = /run/rpc_pipefs
Domain = arhiv.pecar

[Mapping]

Nobody-User = nobody
Nobody-Group = nogroup
EOF
```

Set up Kerberos principals
```
kadmin.local

addprinc -clearpolicy root/admin

addprinc -randkey -clearpolicy host/arhiv.pecar
addprinc -randkey -clearpolicy nfs/arhiv.pecar
ktadd host/arhiv.pecar
ktadd nfs/arhiv.pecar

quit
```

On your client
Note that the principal name _must_ be the first in the client keytab.
```
kadmin -p root/admin

addprinc -randkey -clearpolicy host/tp-2
addprinc -randkey -clearpolicy nfs/tp-2

ktadd -k /tmp/krb5.keytab host/tp-2
ktadd -k /tmp/krb5.keytab nfs/tp-2


addprinc -randkey -clearpolicy tpecar

ktadd -k /tmp/client.keytab tpecar

quit
```

On your client
```sh
# copy over the krb5.conf
scp root@arhiv.pecar:/etc/krb5.conf /tmp/krb5.conf && sudo mv /tmp/krb5.conf /etc/krb5.conf &&
sudo chown root:root /etc/krb5.conf

# determine client keytab
USER_KEYTAB=$(euid=$EUID; eval echo $(krb5-config --defcktname | tr % $ | sed 's/FILE://'))

sudo mv /tmp/krb5.keytab /etc/krb5.keytab &&  # for mount under root user
sudo chown root:root /etc/krb5.keytab &&      # root and kernel processes only (users have their own keytabs)
sudo mkdir -p $(dirname $USER_KEYTAB) &&
sudo mv /tmp/client.keytab $USER_KEYTAB
```

Manually requesting ticket from client (under normal user) should work - this isn't required though, it's queried automatically when the user accesses the NFS share.
```sh
kinit -k -V tpecar -t $USER_KEYTAB

Using default cache: /tmp/krb5cc_1000
Using principal: tpecar@ARHIV.PECAR
Using keytab: /var/lib/krb5/user/1000/client.keytab
Authenticated to Kerberos v5
```
Kinit requires explicit path, others should default to user path.

Host some default directory for initial test
```sh
mkdir /srv/export

cat > /etc/exports <<'EOF'
# /etc/exports: the access control list for filesystems which may be exported
#               to NFS clients.  See exports(5).
#
# Example for NFSv2 and NFSv3:
# /srv/homes       hostname1(rw,sync,no_subtree_check) hostname2(ro,sync,no_subtree_check)
#
# Example for NFSv4:
# /srv/nfs4        gss/krb5i(rw,sync,fsid=0,crossmnt,no_subtree_check)
# /srv/nfs4/homes  gss/krb5i(rw,sync,no_subtree_check)
#
/srv/export *(rw,async,no_subtree_check,no_root_squash,no_all_squash,sec=krb5p:krb5)
EOF
exportfs -arv
```

Try to connect
```sh
sudo pacman -S nfs-utils

sudo mount -vv -t nfs4 -o sec=krb5 arhiv.pecar:/srv/export /mnt

# Modern systems don't require options
sudo mount arhiv.pecar:/srv/export /mnt

# Mounting with no authentication should fail
# sudo mount -t nfs4 -o sec=sys arhiv:/srv/export /mnt
#
# Mounting root is allowed but any resource that is exported with krb5
# will be unavailable without authentication
# sudo mount -t nfs4 -o sec=sys arhiv:/ /mnt
```

Check that idmapd is actually doing it's job with
```sh
sudo nfsidmap -d 
# arhiv.pecar

# list files in mounted export

sudo nfsidmap -l
# should list all users that are owners of files in the listed directory

touch /srv/exports/test
chown tpecar:tpecar /srv/exports/test # or any other user existing on both machines
```
listing files on client would give `tpecar tpecar` if idmapd works, and `nobody nogroup` if not.

---

ID mapping is always done for Kerberos connections, but sec=sys will fall back to UID/GID mapping.

If you want to force it even for AUTH_SYS (breaks compatibility with NFS3)
https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=074b1d12fe2500d7d453902f9266e6674b30d84c

Enable both on server and client via by
```sh
echo "N" | tee /sys/module/nfsd/parameters/nfs4_disable_idmapping

# To make it permanent
echo 'options nfsd nfs4_disable_idmapping=0' > /etc/modprobe.d/nfsd.conf
```

https://unix.stackexchange.com/questions/438939/how-to-get-nfsv4-idmap-working-with-sec-sys/464950#464950

### On principals and user mapping

```
       auth_to_local
              This  tag  allows you to set a general rule for mapping principal names to local user names.  It will be used if there is not an explicit mapping for the principal name that is being trans‐
              lated. The possible values are:

              RULE:exp
                     The local name will be formulated from exp.

                     The format for exp is [n:string](regexp)s/pattern/replacement/g.  The integer n indicates how many components the target principal should have.  If this matches, then a  string  will
                     be  formed from string, substituting the realm of the principal for $0 and the n'th component of the principal for $n (e.g., if the principal was johndoe/admin then [2:$2$1foo] would
                     result in the string adminjohndoefoo).  If this string matches regexp, then the s//[g] substitution command will be run over the string.  The optional g will cause  the  substitution
                     to be global over the string, instead of replacing only the first match in the string.

              DEFAULT
                     The  principal  name will be used as the local user name.  If the principal has more than one component or is not in the default realm, this rule is not applicable and the conversion
                     will fail.

```

### Kerberos troubleshooting

In the (possible) case that you've fudged up the configuration - my suggestion would be to just nuke the keystore and
```
# kdb5_util destroy -f
```
and restart the Kerberos section.

After you reconfigure Kerberos you need to restart the NFS service
```
systemctl restart nfs-server.sevice
```

Make sure you clear all stale tickets on all clients and restart their authentication clients
```
# sudo kdestroy -A
# sudo systemctl restart krb5-kdc.service
# sudo systemctl restart rpc-gssd.service
```

If the client does not have a user to map to, `nfsidmap` will probably map to `nobody 4294967294 (-2)` (this can be seen in journal).
Even after you create the user, you need to clear the current keyring (`nfsidmap -c`) or wait for the current keys to expire (10 mins by default).

If anything additionaly goes wrong take a look at
- Access denied / mount options issue https://unix.stackexchange.com/questions/366636/nfs-kerberos-access-denied-by-server-while-mounting
- https://wiki.linux-nfs.org/wiki/index.php/Nfsv4_configuration
- http://dfusion.com.au/wiki/tiki-index.php?page=Why+NFSv4+UID+mapping+breaks+with+AUTH_UNIX
- https://serverfault.com/questions/532760/why-is-an-nfs-server-mounted-as-rw-returning-read-only-filesystem-errors

An alternate configuration that uses the FreeIPA package (rolling Kerberos, BIND and a high-level command line)
- https://unix.stackexchange.com/questions/396943/fedora-26-nfs-kerberos-preauthentication-failed-mount-lead-to-no-permission

### ZFS exports

On server
```sh
zfs set sharenfs=sec=krb5,rw arhiv-pool/main

# check that it was shared
exportfs -s
```

On client
```sh
# Check that its available
showmount --exports arhiv.pecar

# Mount it
sudo mount arhiv.pecar:/arhiv/ /arhiv/ 
```

As specified at https://github.com/openzfs/zfs/issues/3860

## NFS

On client
```sh
apt install nfs-common

showmount -e arhiv.pecar
```

https://cromwell-intl.com/open-source/performance-tuning/nfs.html

## WebDAV
https://www.comparitech.com/net-admin/webdav/

# Post-install notes

## Grub emits 'invalid environment block' when booting

This is caused by Grub not having write support on ZFS boot volumes, thus it's not able to update the environment block.

Remove the `/boot/grub/grubenv` and the default scripts should skip any environment commands.

> Other, more intrusive workaround, explained in
https://gist.github.com/fire/dce3576b5631b807070e
>
> is to remove / comment out `save_env` invocations in `/etc/grub.d/00_header` and do `sudo grub-update`.

---

Grub also has systemctl modules that try to write to the environment block, you might want to disable those as well

```
sudo systemctl disable grub-common.service
sudo systemctl disable grub-initrd-fallback.service
```

AFAIK despite the scary names, these modues don't do much, they just remove the flags for failed boot. Whether or not you find this acceptable is up to you.

Until grub gets proper ZFS support, grub (or at least its environment block, custom location can be specified via `--file` parameter, see https://unix.stackexchange.com/questions/184871/configure-where-grub2-environment-block-is-located) should reside on a non-ZFS partition.

# Backups

I'll do two different backups of my dataset

## Standard file copy

Since ZFS is still new and I don't really trust any individual component of the system (single point of failure is always a bad idea, and this includes software), I'll do a plain old copy to a mature filesystem (eg. ext4)

```
rsync -a --no-compress --whole-file /PATH_TO_SNAPSHOT /PATH_TO_TARGET --info=progress2 --log-file=/tmp/test-copy.txt
```

## ZFS filesystem transfer

One can replay the filesystem transactions on another dataset, recreating a logical copy on a new drive.

This is TBD.