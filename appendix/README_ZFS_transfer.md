# About

This document is intended to be a sort of a fire-drill for transferring ZFS datasets,
so that in the eventual transfer (be it routine or emergency) we aren't loosing time
figuring out the tools.

## Create test devices

ZFS itself provides direct support for file-backed vdevs.

However, if one wants to emulate more elaborate scenarios, like slow disk accesses, disk errors etc.
one can use device mapper modules.

Provided here are helper routines to manage various device mapper configurations.

```bash
ramdisk_create() {
    # $1 device name, $2 device size
    [[ $# -eq 2 ]] &&

    truncate -s "$2" "/tmp/$1" &&
    losetup -f --show "/tmp/$1"
}

ramdisk_create_delay() {
    # $1 device name, $2 device size, $3 delay in ms
    [[ $# -eq 3 ]] &&

    loop=$(ramdisk_create $1 $2) &&

    dmsetup create "$1" --table "0 $(blockdev --getsz $loop) delay $loop 0 $3" &&
    echo "/dev/mapper/$1"
}

ramdisk_create_flakey() {
    # $1 device name, $2 device size, $3 up interval in s, $4 down interval in s
    [[ $# -eq 4 ]] &&

    loop=$(ramdisk_create $1 $2) &&

    dmsetup create "$1" --table "0 $(blockdev --getsz $loop) flakey $loop 0 $3 $4" &&
    echo "/dev/mapper/$1"
}

ramdisk_cleanup() {
    # $1 device name
    [[ $# -eq 1 ]] &&
    # retrieve backing loop device index
    idx=$(dmsetup table "$1" | cut -d' ' -f 4 | cut -d':' -f 2) && [[ $idx ]] &&

    dmsetup remove $1 &&
    losetup -d "/dev/loop$idx" &&
    rm -i "/tmp/$1"
}
```

## Create test pool

Create a "main" data pool

```bash
# Devices that will hold the main pool
ramdisk_create_delay rmain1 1G 1
ramdisk_create_delay rmain2 1G 1
```

Create the main pool with a mirror vdev, with configuration as set up in [in the main guide](https://gitlab.com/tpecar/arhiv/-/tree/master/#2-set-up-data-store)

```bash
# Create pool, don't mount it's dataset (use it only as a container)
zpool create \
    -o ashift=12 \
    -O acltype=posixacl -O canmount=off -O readonly=on -O compression=lz4 \
    -O dnodesize=auto -O normalization=formD -O relatime=on \
    -O xattr=sa \
    \
    -O mountpoint=/rmain rmain-pool \
    mirror /dev/mapper/rmain1 /dev/mapper/rmain2 \
    -n # dry run, remove this line for actual creation
```

```bash
# Create main dataset on pool
zfs create -o canmount=on -o readonly=off rmain-pool/main

# Make main dataset readable for everyone
chmod ugo+rw /rmain/main
```

```bash
# Provide some data
wget https://cdn.kernel.org/pub/linux/kernel/v6.x/linux-6.1.12.tar.xz -O - | tar -C /rmain/main -xJ
```

## Create backup pool

We create a single disk pool with the same configuration as the main pool.

```bash
# Device that will hold the backup pool
ramdisk_create_delay rbackup 1G 1
```

```bash
zpool create \
    -o ashift=12 \
    -O acltype=posixacl -O canmount=off -O readonly=on -O compression=lz4 \
    -O dnodesize=auto -O normalization=formD -O relatime=on \
    -O xattr=sa \
    \
    -O mountpoint=/backup1 backup1-pool \
    /dev/mapper/rbackup \
    -n # dry run, remove this line for actual creation
```

Note that we don't create any datasets here - `zfs receive` will create the dataset during the initial transfer.

Other transfers will be incremental to this dataset.

## Prepare source for transfer

```bash
# Create recursive snapshot
zfs snapshot -r rmain-pool/main@backup-2023-02-18
```

## Initial transfer

Send initial replicative stream to backup pool, recreating the content (`rmain-pool/main`) under main dataset (`backup-pool/main`).

All received datasets are set up as readonly to prevent modification (user, access time, etc.)

```bash
zfs send -R rmain-pool/main@backup-2023-02-18 | zfs receive -o readonly=on -v backup1-pool/main
```

## Incremental transfer

TBD

## Export of backup pool

```bash
zpool export backup1-pool
```

## Import of backup pool

If one wants to import the pool for the purposes of viewing data / restoring the backup one should mount the _whole pool_ readonly.

This will prevent modification of properties / dropping of datasets etc.

```bash
zpool import -o readonly=on backup1-pool
# If not sure about pool name, `zpool import` will list all pools that can be imported
```

If one wants to update the backup, one should use standard mount.

```bash
zpool import backup1-pool
```
