
Excerpts from server / client configuration in order to confirm configuration issues on the actual server.

Set up 2 machines (one server, one client), both running Ubuntu LTS 20.04, in VirtualBox.

NAT network + host only adapters.

```sh
# Wired connection 1 is host-only (server-client / host)
# Wired connection 2 is NAT (server-client / server-client / internet)
nmcli con mod 'Wired connection 1' ipv4.address 192.168.56.100/24
nmcli con mod 'Wired connection 1' ipv4.gateway 192.168.56.1
nmcli con mod 'Wired connection 1' ipv4.method manual
nmcli con mod 'Wired connection 2' ipv4.dns 192.168.56.200
nmcli con mod 'Wired connection 2' ipv4.ignore-auto-dns yes
nmcli con mod 'Wired connection 1' ipv4.route-metric 200 # must be lower priority than Wired connection 1
nmcli con up 'Wired connection 1'
nmcli con up 'Wired connection 2'
```

```sh
# Set up zones that resolve the Kerberos server (same host)
cat > /etc/bind/named.conf.local <<EOF
// forward zone
zone "arhiv.pecar" {
  type master;
  file "/etc/bind/zones/db.arhiv.pecar";
};

// reverse zone for 192.168.0.0/16
zone "168.192.in-addr.arpa" {
  type master;
  file "/etc/bind/zones/db.192.168";
};
EOF

# Set up zone definitions
mkdir /etc/bind/zones

# Forward zone
cat > /etc/bind/zones/db.arhiv.pecar <<'EOF'
$TTL  604800
@       IN      SOA     arhiv.pecar.  root.arhiv.pecar. (
  2021062100    ; Serial YYYYMMDDnn
      604800    ; Refresh
       86400    ; Retry
     2419200    ; Expire
      604800 )  ; Negative Cache TTL
;
                IN      NS            arhiv.pecar.
arhiv.pecar.    IN      A             192.168.56.200 ; Address of the NS
EOF

# Reverse zone
cat > /etc/bind/zones/db.192.168 <<'EOF'
$TTL  604800
@       IN      SOA     arhiv.pecar.  root.arhiv.pecar. (
  2021062100    ; Serial
      604800    ; Refresh
       86400    ; Retry
     2419200    ; Expire
      604800 )  ; Negative Cache TTL
;
        IN      NS      arhiv.pecar.
200.56  IN      PTR     arhiv.pecar.  ; Address of the NS (subnet portion) - note the LSB to MSB notation!
EOF
```

```
kadmin.local

addprinc -clearpolicy root/admin

addprinc -randkey -clearpolicy host/arhiv.pecar
addprinc -randkey -clearpolicy nfs/arhiv.pecar

addprinc -randkey -clearpolicy host/test-client
addprinc -randkey -clearpolicy nfs/test-client

ktadd host/arhiv.pecar
ktadd nfs/arhiv.pecar

ktadd -k /root/kbclient.keytab host/test-client
ktadd -k /root/kbclient.keytab nfs/test-client
quit
```

On the server side
```
kadmin.local

addprinc root/admin
addprinc -randkey -clearpolicy host/arhiv.pecar
addprinc -randkey -clearpolicy nfs/arhiv.pecar
addprinc -randkey -clearpolicy host/client.pecar
addprinc -randkey -clearpolicy nfs/client.pecar
```

On the client side
```
kadmin -p root/admin

```