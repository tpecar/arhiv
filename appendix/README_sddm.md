
Originally in [README.md](../README.md), _1.6. Set up the desktop environment_

### 1.6.1 Customizing SDDM theme

The following is some additional research that has been done in order to figure out how passwordless login works with SDDM & how to modify the default internal Maui theme

#### Modifying the default SDDM theme

```sh
# https://packages.ubuntu.com/focal/kde/sddm
wget http://archive.ubuntu.com/ubuntu/pool/universe/s/sddm/sddm_0.18.1.orig.tar.gz

tar --strip-components=3 -xv sddm-0.18.1/src/greeter/theme -f ./sddm_0.18.1.orig.tar.gz
```
A quick and dirty way of providing and modifying its subcomponents
(TODO inspect how SddmComponents module gets imported)
```sh
# Extract /components as flat folder
mkdir ./theme/components
tar -C ./theme/components --strip-components=3 -xv sddm-0.18.1/components/ -f ./sddm_0.18.1.orig.tar.gz

# You probably need to rewrok Main.qml `import SddComponents` to `./components`
# in order to modify subcomponents

# Logged-in in a graphical session, you can test the theme with
sddm-greeter --test-mode --theme ./theme

```

#### Modifying the plasma lockscreen (which is separate from sddm)
```sh
# https://packages.ubuntu.com/focal/plasma-workspace
wget http://archive.ubuntu.com/ubuntu/pool/universe/p/plasma-workspace/plasma-workspace_5.18.4.1.orig.tar.xz

tar --strip-components=1 -xv plasma-workspace-5.18.4.1/sddm-theme -f ./plasma-workspace_5.18.4.1.orig.tar.xz
rm sddm-theme/components
ln -s /usr/share/plasma/look-and-feel/org.kde.breeze.desktop/contents/components/ ./sddm-theme/components

# Or extract it from the archive
# tar -C ./sddm-theme/ --strip-components=3 -xv plasma-workspace-5.18.4.1/lookandfeel/contents/components -f ./plasma-workspace_5.18.4.1.orig.tar.xz

```
