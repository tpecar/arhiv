===
NFSv4 wrong effective user / owner, sec=krb5 mount squashes to anonymous user
===

I'm setting up kerberized NFSv4 for personal use
- manually configured NFS, KDC
- no nameservers (using `/etc/hosts` instead), no LDAP
- same users on all machines (not necessarily the same id) and using id mapping for all security modes
  (`nfs4_disable_idmapping` set to 'N')

I've got two machines, both running Ubuntu 20.04 LTS
- `arhiv.pecar` (local address `192.168.56.200`) has the NFS server and the KDC
- `client.pecar` (local address `192.158.56.100`) is the client

---

# The problem

All plumbing seems to work and I can mount the share just fine, but

- if the share is exported with `sec=sys`
  
  server `exportfs -v` output
  ```
  /srv/export     <world>(rw,async,wdelay,no_root_squash,no_subtree_check,sec=sys,rw,secure,no_root_squash,no_all_squash)
  ```
  client `mount` output
  ```
  arhiv.pecar:/srv/export on /mnt type nfs4 (rw,relatime,vers=4.2,rsize=131072,wsize=131072,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=krb5,clientaddr=192.168.56.100,local_lock=none,addr=192.168.56.200)
  ```
  - root has full read / write access
  - other users can read / write files if sufficient privileges are set up
  - `nfsidmap` is active, listing files on the client **properly translates** usernames / groups
  - `chown` from client is **possible**, and properly translates usernames / groups
  
  Files are created under the uid/gid of the **client**, which means they are created with the **wrong** uid / gid on the server

    It gets mapped to the wrong owner if the server happens to have a user with the same uid, otherwise the owner is `nobody:4294967294`
  
  Therefore: The effective user seems to be user specified by the clients uid.



- if the share is exported with `sec=krb5`
  
  server `exportfs -v` output
  ```
  /srv/export     <world>(rw,async,wdelay,no_root_squash,no_subtree_check,sec=krb5p:krb5,rw,secure,no_root_squash,no_all_squash)
  ```
  client `mount` output
  ```
  arhiv.pecar:/srv/export on /mnt type nfs4 (rw,relatime,vers=4.2,rsize=131072,wsize=131072,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=192.168.56.100,local_lock=none,addr=192.168.56.200)
  ```
  - all users have read access, no user (including root) has write access on files / folders owned by them
  - creating files in `o+w` folders will create them under the anonymous user (`nobody:nogroup` or `anonuid:anongid` if specified in exports entry)
  - `nfsidmap` is active, listing files on the client **properly translates** usernames / groups
  - `chown` from client **fails** with `Operation not permitted.`

  Therefore: The effective user seems to be the anonymous user.

I'm at a loss on what could be wrong here, so I'd appreciate the communities insight.

---

# The configuraton

### Users

Both the client and server have the following users
- `test-client-user` different uid/gid on machines (`1000` on server, `1001` on client)
- `test-server-user` different uid/gid on machines (`1001` on server, `1000` on client)
- `testuser` same uid/gid on both machines (`1050`)

### Name resolution

is done via `/etc/hosts` (could also be done via DNS, tested, makes no difference to my problem)
```
127.0.0.1 localhost
192.168.56.200 arhiv.pecar
192.168.56.100 client.pecar
```
forward and reverse mapping works (tested via `resolvectl query HOSTNAME_or_IP`).

### Kerberos configuration
On both machines
`/etc/krb5.conf`
```
[libdefaults]
  default_realm = ARHIV.PECAR

# The following krb5.conf variables are only for MIT Kerberos.
  kdc_timesync = 1
  ccache_type = 4
  forwardable = true
  proxiable = true

[realms]
  ARHIV.PECAR = {
    kdc = arhiv.pecar
    admin_server = arhiv.pecar
  }

[domain_realm]
  .arhiv.pecar = ARHIV.PECAR
  arhiv.pecar = ARHIV.PECAR

[logging]
  kdc          = SYSLOG:DEBUG
  admin_server = SYSLOG:DEBUG
  default      = SYSLOG:DEBUG
```

### Idmapd configuration
On both machines
`/etc/idmapd.conf`
```
[General]

Verbosity = 5
Pipefs-Directory = /run/rpc_pipefs
Domain = arhiv.pecar
Local-Realms = ARHIV.PECAR

[Mapping]

Nobody-User = testuser
Nobody-Group = testuser
```

### NFS configuration
Not sure if it's necessary, saw the options on various mailing lists, but they are mostly undocumented

On both machines
`/etc/default/nfs-common`
```
NEED_GSSD=yes
NEED_IDMAPD=yes
```

### KDC principals

The KDC has the following principals set
```
root@arhiv:~# kadmin.local

Authenticating as principal root/admin@ARHIV.PECAR with password.  

kadmin.local:  listprincs

K/M@ARHIV.PECAR
host/arhiv.pecar@ARHIV.PECAR
host/client.pecar@ARHIV.PECAR
kadmin/admin@ARHIV.PECAR
kadmin/arhiv.pecar@ARHIV.PECAR
kadmin/changepw@ARHIV.PECAR
kiprop/arhiv.pecar@ARHIV.PECAR
krbtgt/ARHIV.PECAR@ARHIV.PECAR
nfs/arhiv.pecar@ARHIV.PECAR
nfs/client.pecar@ARHIV.PECAR
root/admin@ARHIV.PECAR
root@ARHIV.PECAR
```

I'm able to get a ticket on the client
```
root@test-client:~# kinit -k -V nfs/client.pecar

Using default cache: /tmp/krb5cc_0
Using principal: nfs/client.pecar@ARHIV.PECAR
Authenticated to Kerberos v5
```

```
root@test-client:~# klist

Ticket cache: FILE:/tmp/krb5cc_0
Default principal: nfs/client.pecar@ARHIV.PECAR

Valid starting       Expires              Service principal
06/16/2021 13:52:55  06/17/2021 13:52:55  krbtgt/ARHIV.PECAR@ARHIV.PECAR
```

Though I don't need to explicitly call `kinit` myself, `rpc.gssd` seems to probe and authenticate automatically when a valid user-accessible keytab is found.

I've confirmed that it's actually used by removing / renaming the keytab - mount will fail in this case.

### Services

#### On server
```
root@arhiv:~# systemctl show nfs-* --property=Id,ActiveState,SubState,UnitFileState,WantedBy

Id=nfs-client.target
WantedBy=remote-fs.target multi-user.target
ActiveState=active
SubState=active
UnitFileState=enabled

Id=nfs-blkmap.service
WantedBy=nfs-client.target
ActiveState=active
SubState=running
UnitFileState=disabled

Id=nfs-server.service
WantedBy=multi-user.target
ActiveState=active
SubState=exited
UnitFileState=enabled

Id=nfs-idmapd.service
WantedBy=nfs-server.service
ActiveState=active
SubState=running
UnitFileState=static

Id=nfs-utils.service
WantedBy=
ActiveState=inactive
SubState=dead
UnitFileState=static

Id=nfs-config.service
WantedBy=rpc-statd-notify.service nfs-idmapd.service nfs-mountd.service rpc-gssd.service rpc-statd.service nfs-server.service rpc-svcgssd.service
ActiveState=inactive
SubState=dead
UnitFileState=static

Id=nfs-mountd.service
WantedBy=
ActiveState=active
SubState=running
UnitFileState=static
```
```
root@arhiv:~# systemctl show rpc* --property=Id,ActiveState,SubState,UnitFileState,WantedBy

Id=rpcbind.socket
WantedBy=nfs-server.service sockets.target
ActiveState=active
SubState=running
UnitFileState=enabled

Id=rpcbind.target
WantedBy=rpcbind.service
ActiveState=active
SubState=active
UnitFileState=static

Id=rpc-svcgssd.service
WantedBy=auth-rpcgss-module.service
ActiveState=active
SubState=running
UnitFileState=static

Id=rpc-gssd.service
WantedBy=nfs-client.target auth-rpcgss-module.service
ActiveState=active
SubState=running
UnitFileState=static

Id=rpcbind.service
WantedBy=multi-user.target
ActiveState=active
SubState=running
UnitFileState=enabled

Id=rpc-statd.service
WantedBy=
ActiveState=inactive
SubState=dead
UnitFileState=disabled

Id=rpc-statd-notify.service
WantedBy=rpc-statd.service
ActiveState=inactive
SubState=dead
UnitFileState=disabled
```
```
root@arhiv:~# systemctl show krb5* --property=Id,ActiveState,SubState,UnitFileState,WantedBy

Id=krb5-admin-server.service
WantedBy=multi-user.target
ActiveState=active
SubState=running
UnitFileState=enabled

Id=krb5-kdc.service
WantedBy=multi-user.target
ActiveState=active
SubState=running
UnitFileState=enabled
```
```
root@arhiv:~# lsmod | grep nfs

nfsd                  405504  13
auth_rpcgss            94208  3 nfsd,rpcsec_gss_krb5
nfs_acl                16384  1 nfsd
lockd                 102400  1 nfsd
grace                  16384  2 nfsd,lockd
sunrpc                393216  23 nfsd,auth_rpcgss,lockd,rpcsec_gss_krb5,nfs_acl
```
```
root@arhiv:~# lsmod | grep rpc

rpcsec_gss_krb5        40960  6
auth_rpcgss            94208  3 nfsd,rpcsec_gss_krb5
sunrpc                393216  23 nfsd,auth_rpcgss,lockd,rpcsec_gss_krb5,nfs_acl
```

---

#### On client

```
root@client:~# systemctl show nfs-* --property=Id,ActiveState,SubState,UnitFileState,WantedBy

Id=nfs-blkmap.service
WantedBy=nfs-client.target
ActiveState=inactive
SubState=dead
UnitFileState=

Id=nfs-utils.service
WantedBy=
ActiveState=inactive
SubState=dead
UnitFileState=static

Id=nfs-config.service
WantedBy=rpc-gssd.service
ActiveState=inactive
SubState=dead
UnitFileState=static

Id=nfs-client.target
WantedBy=multi-user.target remote-fs.target
ActiveState=active
SubState=active
UnitFileState=enabled
```
```
root@client:~# systemctl show rpc* --property=Id,ActiveState,SubState,UnitFileState,WantedBy

Id=rpcbind.service
WantedBy=multi-user.target
ActiveState=active
SubState=running
UnitFileState=enabled

Id=rpcbind.target
WantedBy=rpcbind.service
ActiveState=active
SubState=active
UnitFileState=static

Id=rpcbind.socket
WantedBy=sockets.target
ActiveState=active
SubState=running
UnitFileState=enabled

Id=rpc-gssd.service
WantedBy=nfs-client.target
ActiveState=active
SubState=running
UnitFileState=static
```
```
systemctl show krb5* --property=Id,ActiveState,SubState,UnitFileState,WantedBy

[no output]
```

While share mounted
```
root@client:~# lsmod | grep nfs

nfsv4                 651264  2
nfs                   303104  2 nfsv4
lockd                 102400  1 nfs
fscache               372736  2 nfsv4,nfs
sunrpc                393216  11 nfsv4,auth_rpcgss,lockd,rpcsec_gss_krb5,nfs
```
```
root@client:~# lsmod | grep rpc

rpcsec_gss_krb5        40960  4
auth_rpcgss            94208  3 rpcsec_gss_krb5
sunrpc                393216  11 nfsv4,auth_rpcgss,lockd,rpcsec_gss_krb5,nfs
```