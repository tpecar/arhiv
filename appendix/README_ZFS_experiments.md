<!-- omit in toc -->
# ZFS Experiments

Various tests and results, mostly just on how to use the `zfs`, `zpool` commands.

<!-- omit in toc -->
## Table of Contents

- [1. Test ZFS on virtual disks](#1-test-zfs-on-virtual-disks)
  - [1.1. Failing drive simulation](#11-failing-drive-simulation)
  - [Further reading](#further-reading)

## 1. Test ZFS on virtual disks

Create a couple of block ramdisk devices and exercise ZFS there to get a hang of it:
```bash
# Create a ramfs mount
mkdir /tmp/test
mount -t ramfs ramfs /tmp/test

# Create a couple sparse files which will be used as ZFS disks
# (can be larger than available memory)
truncate -s 1G /tmp/test/disk1.img
truncate -s 1G /tmp/test/disk2.img
```
One can emulate drive errors or access delays using device mapper
- https://blogs.oracle.com/linux/post/error-injection-using-dm-dust
- https://www.kernel.org/doc/html/v5.5/admin-guide/device-mapper/delay.html

```
# Example: In order to emulate access delay (eg. to see the resilvering process) we can use dm-delay

losetup ramdisk1 /tmp/test/ramdisk1
dmsetup create ramdisk1 --table "0 `blockdev --getsz /dev/loop0` delay /dev/loop0 0 1"

# compare access times
hexdump -C -n 128M /dev/loop0             # immediate
hexdump -C -n 128M /dev/mapper/ramdisk1   # 1ms delay for each access

# use the /dev/mapper/ramdisk in other tests

# remove ramdisk

dmsetup remove ramdisk1
losetup -d /dev/loop0
```

```
# Create ZFS Pool
zpool create mypool mirror /tmp/test/disk1.img /tmp/test/disk2.img

# Create Datasets
zfs create mypool/mydataset1
zfs create mypool/mydataset2
zfs create mypool/mydataset2/mydataset21

# Configure Datasets (can be set on pool/dataset, options are recursively set)
zfs set atime=off mypool                    # disable access time tracking
zfs set exec=off mypool/mydataset1          # disable execution
zfs set quota=500M mypool/mydataset2        # quota (max that can be used by dataset)
zfs set reservation=500M mypool/mydataset2  # reservation (exclusively allocated for the dataset)
zfs set compression=lz4 mypool/mydataset2/mydataset21   # enable lz4 compression for dataset

# Provide some data
cd /mypool/mydataset2/mydataset21
apt source bash
# (since we have enabled compression for that dataset, we can also check the ratio)
zfs get compression,compressratio mypool/mydataset2/mydataset21

# Create a snapshot
zfs snapshot mypool/mydataset2/mydataset21@mysnapshot
zfs list -t snapshot

# Do some change
rm bash*.tar.*
zfs snapshot mypool/mydataset2/mydataset21@mynextsnapshot

# Do an additional change after the second snapshot
touch /mypool/mydataset2/mydataset21/something_new

# Compare current state with snapshots
zfs diff mypool/mydataset2/mydataset21@mynextsnapshot
zfs diff mypool/mydataset2/mydataset21@mysnapshot
zfs diff mypool/mydataset2/mydataset21@mysnapshot mypool/mydataset2/mydataset21@mynextsnapshot

# We can also check the space used by individual snapshot
zfs list -t snapshot

# Whoops ;)
rm -rf /mypool/mydataset2/mydataset21/
ls -la

# We can quickly check the snapshots by accessing the special .zfs folder
# within the dataset root (snapshot is readonly though)
ls -la /mypool/mydataset2/mydataset21/.zfs/snapshot/mynextsnapshot/

# We can revert the dataset back to its snapshot
zfs rollback mypool/mydataset2/mydataset21@mynextsnapshot

# We can spin off a specific snapshot as a separate read-write copy
# (for example, to test/work with previous version)
zfs clone mypool/mydataset2/mydataset21@mysnapshot mypool/mydataset2/mysnapshot_clone
zfs list

# A clone is based on a snapshot - the origin snapshot cannot be removed
zfs get origin mypool/mydataset2/mysnapshot_clone
zfs destroy mypool/mydataset2/mydataset21@mysnapshot

# One can remove the dependency by promoting the clone into an actual dataset
# DO NOTE: This actually MOVES the origin snapshot under the clone
zfs promote mypool/mydataset2/mysnapshot_clone
zfs get origin mypool/mydataset2/mysnapshot_clone

# We can rename it while we're at it
zfs rename mypool/mydataset2/mysnapshot_clone mypool/mydataset2/mydataset22
zfs list

# Lets do a big whoopsie
# NOTE: Really not recommended on actual data
zpool destroy mypool
zfs list
zpool list

# If we didn't do too much stupid shit, we can check if the pool is importable
zpool import -D -d /tmp/test
# And we can recover it
zpool import -D -d /tmp/test mypool
zfs list

# Check memory usage of disk images with
du -h --apparent-size /tmp/test   # apparent file usage
du -h /tmp/test                   # actual file (memory) usage
```

Additional stuff todo
```bash
# import pool and mount read only (-o is for specifying non-persistent, per import options)
zpool import -o readonly=on mypool
```

### 1.1. Failing drive simulation

https://www.kernel.org/doc/html/latest/admin-guide/device-mapper/dm-dust.html

_References:_
- https://wiki.archlinux.org/index.php/ZFS/Virtual_disks
- https://www.jamescoyle.net/knowledge/951-the-difference-between-a-tmpfs-and-ramfs-ram-disk

### Further reading
- https://jrs-s.net/2015/02/06/zfs-you-should-use-mirror-vdevs-not-raidz/
- https://www.mbeckler.org/blog/?p=595
- https://blog.programster.org/zfs-add-intent-log-device
- https://superuser.com/questions/927753/would-a-zfs-mirror-with-one-drive-mostly-offline-work

Some additional material for fun & profit:
- https://medium.com/oracledevs/mirroring-a-running-system-into-a-ramdisk-5f2e77248a92

