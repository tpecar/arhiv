# Minimal Debian+KDE+i3 installation

This was originally part of the README.md but we have switched to a customized Ubuntu Server installation.

This installation procedure doesn't set up some of the advanced features such as ZFS root + root snapshots.

Nevertheless, it's still a pretty good guide for a minimal system.

## 1. Do a minimal Debian installation

Get the current stable Debian *netinst* image here \
https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/

Boot via UEFI, select text mode install
Configure as following:
- Language `English`
- Location `Slovenia`
- Keyboard `Slovenian`
- Hostname `arhiv`
- (get connected to the internet)
- Root password `[as noted on machine]` - should be at least strong enough to prevent bots from logging in, in case the machine gets exposed on the net and other measures fail. Ideally, all servers should disallow root logins. However, we keep root enabled to allow for single user mode in case of configuration f*ckups.
- User account `arhiv`, password `arhiv` - this is the default user for local file management. This user is capable of managing the on-line filesystem, but shouldn't have the capabilities to manage the ZFS (can't do / change snapshots, modify cluster). This user doesn't have sudo. Ideally, the servers should disallow remote (origin == gateway) logins of this user.
- In partitioner
  - select Guided - Use entire disk
  - select SSD as target disk
  - select `All files in one partition`
  - keep the partitioning as-is (even swap)
- Configure the package repository to something local (used ftp.si.debian.org in my case)
- In software selection, select only `standard system utilities`

The system should install and reboot, dropping you at tty login.

_References:_
- https://haydenjames.io/linux-performance-almost-always-add-swap-space/

## 2. Initial configuration

Login as **root**.

Before anything else, configure the shell and editors:
```bash
apt install vim
echo 'export EDITOR=vim' > /etc/profile.d/default_editor.sh
```

Modify `/root/.bashrc` to increase history size (so you have the whole system setup/maintanence logged).
```bash
HISTSIZE=10000
HISTFILESIZE=10000
```

## 3. Set up Xorg + XDM + Minimal KDE + i3

### Install packages

```bash
apt install xserver-xorg-core xserver-xorg-video-intel xserver-xorg-input-evdev x11-xserver-utils x11-xkb-utils xterm x11-utils --no-install-recommends
apt install xfonts-terminus xfonts-75dpi xfonts-100dpi
apt install xdm
apt install i3 suckless-tools feh
apt install kde-plasma-desktop systemsettings kscreen ksysguard --no-install-recommends
```

_References:_
- https://wiki.debian.org/Xorg
- https://gist.github.com/ryot4/0712f02f709be90bd5d6812b85e3b529

### Configure X11

Need to set up font paths @ [/etc/X11/xorg.conf.d/load-fonts.conf](/etc/X11/xorg.conf.d/load-fonts.conf)

Check with `xlsfonts` / `xfontsel` that they are getting picked up.

_References:_
- https://wiki.archlinux.org/index.php/fonts

### Configure XDM
Login screen is managed @ [/etc/X11/xdm/Xresources](/etc/X11/xdm/Xresources)

We're using Xft font specification instead of XLFD.

Any additional X server parameters (for example, DPI ajdustment, TTY used by xdm) can be specified at `/etc/X11/xdm/Xservers`

DE startup is done per-user
- root [/root/.xsession](/root/.xsession) - minimal environment, i3 only
- arhiv [/home/arhiv/.xsession](/home/arhiv/.xsession) - KDE with i3 window manager

_References:_
- https://wiki.archlinux.org/index.php/XDM
- https://wiki.archlinux.org/index.php/X_Logical_Font_Description
- http://cafim.sssup.it/~giulio/other/Customization_XDM.html
- https://medium.com/hacker-toolbelt/i3wm-on-debian-10-buster-c302420853b1

### Configure i3

Root configuration @ [/root/.i3/config](/root/.i3/config) \
Configuration for other users @ [/etc/i3/config](/etc/i3/config)

If you want some other background, see \
https://www.toptal.com/designers/subtlepatterns/

_References:_
- https://i3wm.org/docs/userguide.html
- https://userbase.kde.org/Tutorials/Using_Other_Window_Managers_with_Plasma
- https://ryanlue.com/posts/2019-06-13-kde-i3
- https://github.com/heckelson/i3-and-kde-plasma

### Configure KDE

In order to remove the 30s spinning screen at login, go to \
System Settings > Apperance > Workspace Theme > Splash Screen \
and set it to None

To get the KDE pager play nicely with i3, configure
- Pager > General > Display: _Desktop name_
- Task Manager > General > Filters > _Show only tasks from current desktop_

_References:_
- https://forum.kde.org/viewtopic.php?f=17&t=133851

## 4. Install ZFS

Enable _contrib_ and _buster-backports_ components
```bash
sed -i 's/main/main contrib/' /etc/apt/sources.list
sed -i 's/\(\(deb http:.*\)buster\( main.*\)\)/\1\n\2buster-backports\3/' /etc/apt/sources.list
apt update
```

Install ZFS and its dependencies
```bash
apt install linux-headers-`uname -r`
apt install -t buster-backports dkms spl-dkms
apt install -t buster-backports zfs-dkms zfsutils-linux
```

And restart the system so that the kernel module gets loaded + zpool, zfs commands become available.

_References:_
- https://wiki.debian.org/ZFS
- https://wiki.debian.org/SourcesList
- https://wiki.debian.org/Backports
