<!-- omit in toc -->
# ZFS

Originally part of README.md, outgrew it completely.

It's the documentation I wish I had when I started, and may serve as a guide for someone just starting out.

> It's still a little rough around the edges, but I'll try to fix it up in the future.
> 
> PRs welcome.

<!-- omit in toc -->
## Table of Contents

- [1. Learn about ZFS](#1-learn-about-zfs)
- [2. Which ZFS exactly?](#2-which-zfs-exactly)
- [3. Get familiar with the subject matter](#3-get-familiar-with-the-subject-matter)
  - [3.1. Reference documentation](#31-reference-documentation)
  - [3.2. Tutorials and presentations](#32-tutorials-and-presentations)
  - [3.3. Misconceptions, warnings, rants](#33-misconceptions-warnings-rants)
  - [3.4. Miscellania](#34-miscellania)
- [4. Zsys](#4-zsys)
  - [Documentation](#documentation)
  - [General usage](#general-usage)
  - [Troubleshooting](#troubleshooting)
  - [Known bugs](#known-bugs)
    - [Saving a state with spaces in name breaks boot menu generation](#saving-a-state-with-spaces-in-name-breaks-boot-menu-generation)
    - [Reverting between clones will cause zfs-mount.service to fail](#reverting-between-clones-will-cause-zfs-mountservice-to-fail)
- [5. Syncing ZFS filesystems](#5-syncing-zfs-filesystems)

## 1. Learn about ZFS

ZFS is, quite literally, the [systemd of filesystems](https://www.youtube.com/watch?v=o_AIw9bGogo).

To get a rough scale of it's size, with ZFS you get:
- a volume manager, which handles JBOD, RAID 0, 1, [5/6/7](https://www.klennet.com/notes/2019-07-04-raid5-vs-raidz.aspx) and provides striping
- a filesystem, which provides
  - dynamic allocation of partitions, which can be nested, have snapshot management and can have CoW clones
  - quota management based on user, group or [project](https://superuser.com/questions/1441509/what-are-project-ids-in-linux-as-mentioned-in-the-chattr-manual)
  - multiple ACLs
  - compression and deduplication of data
  - encryption with its own key management
  
  You can also (and in some cases are required to) manually tweak various internal components, which you wouldn't touch with a 20ft pole in other filesystems, like
  - manage various level of caches, some of which can reside on drives
  - manually manage metadata and journals
  - manually configure checksum, compression and encryption algorithms, on a per-partition basis
  
  And if that's not enough, you can allocate volumes (ie. block devices) _on the filesystem_, which you can format to other filesystems.

You also get
- a mountpoint manager that works independently of `/etc/fstab`
- a network share manager that works independently of `/etc/exports`
- a kitchen sink (not sure about OpenZFS, but I'm pretty sure you get one with Oracle)

---

So ZFS is a misnomer in a way, since the name doesn't represent **_just_** the filesystem but rather a _collection_ of highly integrated tools to manage data storage.

---

Taking up ZFS means that you are taking up a whole new world of tools, concepts and practices
that you'll fight against until you actually take the time to understand it.

However, once you get the hang of it, it provides good integration between all components relevant to the filesystem.

## 2. Which ZFS exactly?

The version history of ZFS is, as is with all enterprise technology coming from Sun, rather messy.

During Sun's development, filesystem compatiblity was denoted with version numbers.

Volume manager (pool) and the filesystem have separate version numbers. OpenSolaris codebase, part of which was ZFS, and from which all open-source variants are derived, used pool version 28, filesystem version 5.

Shortly after Oracle acquired Sun in 2010, it pulled the plug on the OpenSolaris project.

Multiple entities (some commercial) then forked ZFS and provided their own additions to the technology, ports to different systems, etc., so we ended up with multiple, somewhat incompatible variants of ZFS.

The general consensus today is that
- all open-source versions of ZFS will be able to read pool version 28, filesystem version 5, and further developments use pool version 5000, with the functionality denoted via feature flags
  - how this came to be http://web.archive.org/web/20160419064650/http://blog.delphix.com/csiden/files/2012/01/ZFS_Feature_Flags.pdf
  - the current state https://openzfs.github.io/openzfs-docs/Basic%20Concepts/Feature%20Flags.html
- ZFS pools are by default created with all features (supported by the variant) enabled, but this can be configured at pool creation.
- Once a feature is enabled, it cannot be disabled.
- Pools can only be imported if the ZFS variant supports all enabled feature flags.

Oracle Solaris [continues to use version numbers](https://docs.oracle.com/cd/E37838_01/html/E61017/appendixa-1.html), and its ZFS is incompatible with open-source variants.

BSD and Linux have recently merged their efforts under [OpenZFS](https://openzfs.org/wiki/Main_Page). **This is the version we'll be using.**

> Due to license incompatibilities, the CDDL licensed derivatives of OpenSolaris' ZFS, like OpenZFS, cannot be included into the Linux kernel.
> 
> OpenZFS on Linux is thus provided as a [DKMS module](https://wiki.archlinux.org/title/Dynamic_Kernel_Module_Support).
> 
> Most distributions provide it in source form, while Ubuntu provides it as a pre-compiled binary. In both cases, you get a loadable kernel module which, [as Canonical argues](https://ubuntu.com/blog/zfs-licensing-and-linux), doesn't violate the GPLv2 license.

References:
- [https://en.wikipedia.org/wiki/ZFS#Oracle_Corporation,_closed_source,_and_forking_(from_2010)](https://en.wikipedia.org/wiki/ZFS#Oracle_Corporation,_closed_source,_and_forking_(from_2010))
- https://en.wikipedia.org/wiki/OpenZFS#Releases_and_feature_histories
- https://papers.freebsd.org/2019/BSDCan/jude-The_Future_of_OpenZFS_and_FreeBSD.files/jude-The_Future_of_OpenZFS_and_FreeBSD.pdf

## 3. Get familiar with the subject matter

There's a ton of blogs / articles etc. describing the benefits of ZFS, but a few of them do a good job explaining its concepts, much less on how to properly administer it.

A quick rundown before you delve into documentation (don't expect to understand everything at this point):

- virtual device or **vdev** is a collection of block devices
- vdevs are collected into a **pool**
- _vdev_ (can) provide redundancy, _pool_ provides striping across _vdevs_
- loosing a single _vdev_ (that is, surpassing the redundancy of a _vdev_) means loosing the entire _pool_
- a _pool_ represents a storage space on which the **filesystem** is allocated
- a **filesystem** can contain standard files and special ZFS objects - **datasets**, which are
  - **filesystem** - they can thus be nested
    - we configure [properties](https://openzfs.github.io/openzfs-docs/man/8/zfsprops.8.html) on the filesystem-level
    - objects inside the filesystem (children) inherit its properties
    - we can leave the filesystem unmounted and still mount its children \
      The unmounted filesystem can be used solely as a container for setting properties

  - **snapshot** is a read-only view of the filesystem at a certain point in time
    - a **clone** is a read-write filesystem created from a _snapshot_ (the initial state or the **origin** of the clone)
    - both the original _filesystem_ as its _clone_ can diverge from the snapshot
    - the _clone_ can replace (be _promoted_ to) the original filesystem - they exchange names / paths, so that the data referenced by the _clone_ is available under the original filesystems' name (and vice versa)

    ```
    A : filesystem A
      A@snap -> snapshot of A
                 -> B: clone of snapshot of A: filesystem ~A
  
    A references initial filesystem, B the clone
    ----
    [we promote the clone]
    ----
    
    A : filesystem ~A
      A@snap -> snapshot of ~A
                 -> B: clone of snapshot of ~A: filesystem A
    
    A now references the clone, B the initial filesystem
    ```
  - **bookmark** of a snapshot tells that the snapshot exists / existed without referencing its data, for maintaining version history
  - **volume** represents a virtual block device, allocated on the filesystem. It's essentially a loopback device managed by ZFS, which can have _snapshots_, _bookmarks_, _clones_
- datasets can be synchronized across filesystems, pools, possibly on different machines via `zfs send` / `zfs receive`
  - these are dumb commands which simply serialize / deserialize and replay transactions on a different filesystem
  - they can be destructive
  - storing the serialized stream is generally a _bad idea_, as the serialization format is not stable, and has no recovery mechanisms. \
    It is _only_ intended for replicating datasets between servers (preferrably running the same version of ZFS) over a reliable network channel. \
    It is _not_ designed for tape backups.

### 3.1. Reference documentation

A core reference for the current state-of-the art should be the manpages
- https://openzfs.github.io/openzfs-docs/man/8/zpoolconcepts.8.html
- https://openzfs.github.io/openzfs-docs/man/8/zfsconcepts.8.html
- https://openzfs.github.io/openzfs-docs/man/8/zfsprops.8.html
- https://openzfs.github.io/openzfs-docs/man/8/zpool.8.html
- https://openzfs.github.io/openzfs-docs/man/8/zfs.8.html

_I highly suggest_ that you force yourself to go through `zpoolconcepts`, `zfsconcepts` and possibly `zfsprops` before going through any other documentation.

Even if they seem overwhelming at first, all other material derives from these concepts and you'll get to the whole picture faster.

---

Additionally, a comprehensive document with examples, which isn't an eyesore to read, is provided by Illumos (inherited from Sun)
https://illumos.org/books/zfs-admin/preface.html#preface

It's from 2008, and was written for Solaris (so any functionality added via feature flags isn't present), but the concepts and the commands have stayed the same, and should provide a solid base. 

### 3.2. Tutorials and presentations

Go through the introductory guides to get the idea of the concepts:
- [Matt Ahrens - OpenZFS, MeetBSD 2016](https://www.youtube.com/watch?v=Hz7CEI8LwSI) - history and reasons for ZFS by its developers \
  Spiritually connected to [this presentation](https://www.snia.org/sites/default/orig/sdc_archives/2008_presentations/monday/JeffBonwick-BillMoore_ZFS.pdf) which is a bit technical but kind of a classic.
- [ZFS for dummies](https://blog.victormendonca.com/2020/11/03/zfs-for-dummies/) - quick terminology, diagrams and usage on a single page
- [Arstechnica - Understanding ZFS Storage](https://arstechnica.com/information-technology/2020/05/zfs-101-understanding-zfs-storage-and-performance/) a deeper dive into ZFS structure
- [linux.conf.au - "The ZFS filesystem" - Philip Paeps (LCA 2020)](https://www.youtube.com/watch?v=Hjpqa_kjCOI) - [presentation slides](https://papers.freebsd.org/2020/linux.conf.au/paeps_The_ZFS_filesystem.files/paeps_The_ZFS_filesystem.pdf) - a compressed technical tour and usage
- [ZFS on Linux the Billion dollar file system](https://www.youtube.com/watch?v=n0Uskl9fcKI) - a very thorough, step by step tutorial if you were overwhelmed by previous presentations
- [ZFS on Linux 2](https://www.youtube.com/watch?v=z7W5-QLpm34) - an addition to the previous tutorial, covering more complex topics like ACLs

At this point most presentations will start to repeat themselves.

### 3.3. Misconceptions, warnings, rants

The classic ones:
- http://nex7.blogspot.com/2013/03/readme1st.html
- https://jrs-s.net/2015/02/03/will-zfs-and-non-ecc-ram-kill-your-data/
- https://jrs-s.net/2015/02/06/zfs-you-should-use-mirror-vdevs-not-raidz/
- https://serverfault.com/questions/511154/zfs-performance-do-i-need-to-keep-free-space-in-a-pool-or-a-file-system

Things that might surprise you
- https://louwrentius.com/the-hidden-cost-of-using-zfs-for-your-home-nas.html
- https://unix.stackexchange.com/questions/276600/how-to-move-files-from-one-zfs-filesystem-to-a-different-zfs-filesystem-in-the-s
- https://www.reddit.com/r/zfs/comments/fvjmmb/is_moving_files_between_datasets_a_copy_operation/

### 3.4. Miscellania

This is a staging area for various additional material that I don't exactly know what to do with right now.

Some additional thorough guides
- http://wiki.bdnog.org/lib/exe/fetch.php/bdnog9/zfs_intro2.pdf
- https://www.math.utah.edu/~beebe/talks/2017/zfs/zfs.pdf \
  Have a look at its references

RAIDZ explanations and tuning
- https://static.ixsystems.co/uploads/2020/09/ZFS_Storage_Pool_Layout_White_Paper_2020_WEB.pdf
- https://www.delphix.com/blog/delphix-engineering/zfs-raidz-stripe-width-or-how-i-learned-stop-worrying-and-love-raidz

---

A random assortment of technical information of ZFS operation and on-disk structures

https://openzfs.org/wiki/Developer_resources

however, most documentation is largely outdated.

---

## 4. Zsys

> This project + the whole ZFS integration seems to have been dropped by Ubuntu / Canonical
> 
> https://www.phoronix.com/news/Ubuntu-Zsys-0.5.9
>  
> https://bugs.launchpad.net/ubuntu/+source/ubiquity/+bug/1968150
> 
> While the zsys project is a neat demonstration of what can be done with ZFS as the root filesystem, it did not reach a state of maturity at which I would be comfortable at letting it manage my personal, much less business-critical data.
> 
> I sincerely hope this will change in the future.

The whole point of using ZFS on root is to have the snapshot ability - so we can always revert the whole system to the last sane state.

System snapshots can be done manually via the `zfs` command
- https://ubuntu.com/blog/deploying-ubuntu-root-on-zfs-with-maas
- https://ubuntu.com/tutorials/using-zfs-snapshots-clones#1-overview

Zsys is a Ubuntu-specific set of tools for managing system snapshots in a manner similar to Windows(TM) restore points.

It sets up the boot menu, so that you can simply choose the system snapshot you want to boot into.

*Please note* that having a good understanding of zsys _is_ required if you're going to do custom dataset hierarchies - misconfigurations can even get your data destroyed by the Zsys garbage collection service

General rules of thumb
- *do not* make custom datasets under `/USERDATA/` of *any* pool
- *do not* set `com.ubuntu.zsys` properties (especially `bootfs`) on custom datasets
- all datasets that should be left alone by Zsys *should* reside *outside* `/BOOT/`, `/ROOT/`, `/USERDATA/` datasets of *any* pool

Full details available here https://github.com/ubuntu/zsys/issues/103

### Documentation

The project is currently lacking official documentation but most of the internals and principles of operation are available on the developers blog
https://didrocks.fr/

The quick summary I can provide after reading his articles is
- zsys tries to handle system / user data snapshots in a system agnostic way (so it has potential to be used outside Ubuntu)
- zsys manages snapshot on
  - 'machine' level : a machine is a single installation, there can be multiple on the same system (multiboot) and they can share datasets
  - system level : one can revert the root portion of the filesystem without reverting user data
  - system + user level : both system and user datasets can be reverted to same time frame
- reverting is done via dataset cloning + mounting the clones, previous datasets are retained (no ZFS snapshot rollbacks, no clone promotions) and can be booted again into
  - reverting to same state always creates new clones (you always start from the same point)
  - clones are added to the boot menu (you can continue where you left off with the reverted state)
- datasets are separated into
  - Boot dataset: on `bpool/BOOT/` (separate pool which has a subset of feature flags in order to be readable by GRUB)
  - System datasets: datasets in `rpool/ROOT/`
  - User datasets: datasets in `rpool/USERDATA`
  - Persistent datasets: all datasets outside `/BOOT/`, `/ROOT/`, `/USERDATA/`

  All of them need to be named `[alpha name, 'ubuntu' by default]_[6 character lowercase alphanumeric]` for zsys to properly handle reverts.

- all information required for managing snapshots is provided by
  - `/etc/zfs/zpool.cache` current boot
  - [user properties on datasets](https://didrocks.fr/2020/06/19/zfs-focus-on-ubuntu-20.04-lts-zsys-properties-on-zfs-datasets/)
    - system datasets have `com.ubuntu.zsys:bootfs` which determines if the dataset has to be mounted
    - user datasets have `com.ubuntu.zsys:bootfs-datasets` which references the system dataset to which they belong

There is also the whole topic of automatic snapshots and garbage collection which I didn't touch but this should be enough for an intro. Explore the blog for the nitty gritty details.

Additional articles on the topic
- https://arstechnica.com/gadgets/2020/03/ubuntu-20-04s-zsys-adds-zfs-snapshots-to-package-management/

### General usage

Every `apt install` invocation creates a new snapshot.

You can force a manual snapshot via `zsysctl save --system NameOfYourSnapshot` (`--system` does a snapshot of both the root & user data. Without the switch only user data is backed up).

### Troubleshooting

A quick rundown to figure out what went wrong:
- normal boot & revert are handled and can fail in [`zfs-mount-generator`](https://manpages.ubuntu.com/manpages/focal/man8/zfs-mount-generator.8.html) which sets up mountpoints based on `/etc/zfs/zfs-list.cache/*`
- the script generates a log file at `/tmp/zsys-revert-out.log`, this might contain additional error logs / stacktraces not present in journal
- one can run the script manually with verbose mode `bash -x /lib/systemd/system-generators/zfs-mount-generator`, you will most likely end up in `drop_emergency_on_failure()`

Note that simply rebooting should take the current system state instead of the snapshot (clone), if it's is still in viable state, and that will give you back a functional system.

### Known bugs

#### Saving a state with spaces in name breaks boot menu generation

Thus making the system unbootable.

Resolution: livecd boot, chroot, remove / rename the offending snapshot and recreate the boot menu

https://github.com/ubuntu/zsys/issues/169

#### Reverting between clones will cause zfs-mount.service to fail

If you only have `bootfs` mounts you probably won't even notice anything is wrong, otherwise all late-boot mounts performed by zfs (and not zsysctl) will fail to mount automatically.

This happens because zsysctl fails to unset the `canmount` property (from `on` to `noauto`) from current clone before transitioning to some other existing cloned state.

The workaround is to manually unset the property of the offending snapshot and reboot, which should fix the issue. This is, however, annoying.

- Determines current boot system datasets https://github.com/ubuntu/zsys/blob/c01068b0fc9ea049a1848dac101175af33b42f7c/internal/machines/cmdline.go#L45
- https://github.com/ubuntu/zsys/blob/c01068b0fc9ea049a1848dac101175af33b42f7c/internal/machines/boot.go#L78
- https://github.com/ubuntu/zsys/blob/c01068b0fc9ea049a1848dac101175af33b42f7c/internal/machines/boot.go#L289

As far as I understand the code, there are two different code paths, depending on whether

- we boot into snapshot - this creates new clones from the snapshot, re-tags all existing datasets based on whether they belong to this "set" (set of dataset collectively representing the root filesystem, zsys community has a tendency to split stuff) and promotes the clone in case of successful boot (? based on comments, didn't find the actual algo)
  
  This works as expected.

- we boot into the existing clone - this fails to re-tag the previously active clone to `noauto`, keeping it `on`

## 5. Syncing ZFS filesystems

For now just a dump of interesting articles:
- https://arstechnica.com/information-technology/2015/12/rsync-net-zfs-replication-to-the-cloud-is-finally-here-and-its-fast/
- https://github.com/zrepl/zrepl/issues/86
- https://zfs-discuss.opensolaris.narkive.com/k0wVM7ca/how-to-use-mbuffer-with-zfs-send-recv
- https://github.com/zrepl/zrepl
- https://github.com/jimsalterjrs/sanoid
