Well, I've figured out that it's worthwhile to understand _how_ NFS and Kerberos deal with users, and how principal names come to play here - topics that are frequently left out in most guides.

Below is a summary and a quick demo on how to properly configure mapping for a user over kerberized NFS.

---

In kerberized NFS, one needs to be aware of the difference between
- **the effective user executing the file operation**

  The effective (server-local) user of the file operation is determined by Kerberos' [local authorization interface](https://web.mit.edu/kerberos/krb5-1.13/doc/plugindev/localauth.html), which is [configured via `auth_to_local` tag](https://web.mit.edu/kerberos/krb5-1.12/doc/admin/conf_files/krb5_conf.html#realms), and if none given, defaults to `auth_to_local = DEFAULT`, which is [defined as](https://web.mit.edu/kerberos/krb5-1.12/doc/admin/conf_files/krb5_conf.html#realms)

  > The principal name will be used as the local user name. If the principal has more than one component or is not in the default realm, this rule is not applicable and the conversion will fail.

  In the case of conversion failure it assumes the anonymous user (`nobody:nogroup` or `anonuid:anongid` if specified in exports entry)

  [Related discussion that provides a good explanation.](https://docs.pagure.org/sssd.sssd/design_pages/nss_with_kerberos_principal.html)

- **the parameters / results of this file operation**
  
  When the NFS server performs the file operation
  1. parameters need to be mapped from client-provided users to server-local users (`chown` command)
  2. results need to be mapped back to client-local users (`ls` command)
  
  This is handled by the `id_resolver` upcall program specified in `/etc/request-key.conf` or `/etc/request-key.d/*` (usually `nfsidmap`).
  
  Usernames are transferred between hosts as `user@dns_domain` strings and are mapped to local uid/gid on each side.

The effective user performing the file operation is derived from the ticket (thus it matters under which principal we authentecate), and _NOT_ from the uid of the locally running process requesting the file operation (which one might naively expect).

---

So, in order for the effective user mapping to work, one needs to create a `username` principal (effectively `username@REALM`) or, if a more complex principal name is used, provide an appropriate `auth_to_local` mapping in `/etc/krb5.conf`, as described here
- https://access.redhat.com/articles/4040141
- https://blog.samoylenko.me/2015/04/15/hadoop-security-auth_to_local-examples/
- https://community.cloudera.com/t5/Community-Articles/Auth-to-local-Rules-Syntax/ta-p/245316

---

Each user _should have its own_ default principal (`user/client-fqdn`), which is used for mapping to server-local user.

All users on single client _can share_ the service principal (`nfs/client-fqdn`)

The following configuration was tested on CentOS systems configured via this guide (guide intended for Centos 7, tested on 8, minor additions were needed)

https://www.linuxhelp.com/how-to-set-up-nfs-server-with-kerberos-based-authentication

```sh
# Start kadmin on client machine (I assume that you have the root/admin principal already set up)
# and create the testuser principal and relevant keytabs
#
# I've opted for password authentication for testuser, but you can use keytabs.
#
[root@nfsclient /]# kadmin -p root/admin
Authenticating as principal root/admin@KDC.COM with password.
Password for root/admin@KDC.COM: 
kadmin:  addprinc testuser
No policy specified for testuser@KDC.COM; defaulting to no policy
Enter password for principal "testuser@KDC.COM": 
Re-enter password for principal "testuser@KDC.COM": 
Principal "testuser@KDC.COM" created.
kadmin:  ktadd -k /tmp/testuser.client.keytab nfs/nfsclient.kdc.com
Entry for principal nfs/nfsclient.kdc.com with kvno 3, encryption type aes256-cts-hmac-sha1-96 added to keytab WRFILE:/tmp/testuser.client.keytab.
Entry for principal nfs/nfsclient.kdc.com with kvno 3, encryption type aes128-cts-hmac-sha1-96 added to keytab WRFILE:/tmp/testuser.client.keytab.
Entry for principal nfs/nfsclient.kdc.com with kvno 3, encryption type arcfour-hmac added to keytab WRFILE:/tmp/testuser.client.keytab.
Entry for principal nfs/nfsclient.kdc.com with kvno 3, encryption type camellia256-cts-cmac added to keytab WRFILE:/tmp/testuser.client.keytab.
Entry for principal nfs/nfsclient.kdc.com with kvno 3, encryption type camellia128-cts-cmac added to keytab WRFILE:/tmp/testuser.client.keytab.
kadmin:  quit

[root@nfsclient /]# mkdir -p /var/kerberos/krb5/user/$(id -u testuser)/
[root@nfsclient /]# mv /tmp/testuser.client.keytab /var/kerberos/krb5/user/$(id -u testuser)/client.keytab
[root@nfsclient /]# chown testuser:testuser /var/kerberos/krb5/user/$(id -u testuser)/client.keytab
[root@nfsclient /]# ls -la /var/kerberos/krb5/user/$(id -u testuser)/client.keytab
-rw-------. 1 testuser testuser 394 Jun 17 06:04 /var/kerberos/krb5/user/1051/client.keytab

# Mount the filesystem
# I assume here that you've already set up
#   host/nfsserver.kdc.com
#   nfs/nfsserver.kdc.com
#
#   host/nfsclient.kdc.com
#   nfs/nfsclient.kdc.com
#
# copied the relevant configurations and default keytabs (either manually or via kadmin) to both server and client
# and started the necessary services.
mount nfsserver.kdc.com:/kerberos /mnt

# Test the user
[root@nfsclient /]# sudo -i -u testuser

# Authenticate with the default principal for the user (testuser@KDC.COM)
[testuser@nfsclient ~]$ kinit -V 
Using default cache: 1051
Using principal: testuser@KDC.COM
Password for testuser@KDC.COM: 
Authenticated to Kerberos v5

# Check that the default principal is actually testuser@KDC.COM
[testuser@nfsclient ~]$ klist
Ticket cache: KCM:1051
Default principal: testuser@KDC.COM

Valid starting       Expires              Service principal
06/17/2021 06:12:22  06/18/2021 06:12:14  krbtgt/KDC.COM@KDC.COM
        renew until 06/17/2021 06:12:22

# Check that the users client.keytab, containing key for nfs/nfsclient.kdc.com
# - clients NFS service principal - is being picked up
# (can access nfs share)
#
# and that the correct effective user is now used for file operation
# (derived from default principal and mapped to server-local user)
# - the server should also have a `testuser` user
#
[testuser@nfsclient /]$ touch /mnt/testfile
[testuser@nfsclient /]$ ls -la /mnt/testfile
-rw-rw-r--. 1 testuser testuser 0 Jun 17  2021 /mnt/testfile
```