addprinc -randkey -clearpolicy host/nfsclient
addprinc -randkey -clearpolicy nfs/nfsclient

ktadd -k /tmp/krb5.keytab host/nfsclient
ktadd -k /tmp/krb5.keytab nfs/nfsclient


addprinc -randkey -clearpolicy testuser

ktadd -k /tmp/client.keytab testuser


root@nfsclient ~]# mv /tmp/krb5.keytab /etc/krb5.keytab
[root@nfsclient ~]# chown root:root /etc/krb5.keytab
[root@nfsclient ~]# mv /tmp/client.keytab /var/kerberos/krb5/user/$(id -u testuser)/client.keytab
[root@nfsclient ~]# chown testuser:testuser /var/kerberos/krb5/user/$(id -u testuser)/client.keytab