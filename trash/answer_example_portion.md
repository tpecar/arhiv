The following configuration was tested on CentOS systems configured via this guide (guide intended for Centos 7, tested on 8, minor additions were needed)

https://www.linuxhelp.com/how-to-set-up-nfs-server-with-kerberos-based-authentication

```sh
# Start kadmin on client machine (I assume that you have the root/admin principal already set up)
# and create the testuser principal and relevant keytabs
#
# I've opted for password authentication for testuser, but you can use keytabs.
#
[root@nfsclient /]# kadmin -p root/admin
Authenticating as principal root/admin@KDC.COM with password.
Password for root/admin@KDC.COM: 
kadmin:  addprinc testuser
No policy specified for testuser@KDC.COM; defaulting to no policy
Enter password for principal "testuser@KDC.COM": 
Re-enter password for principal "testuser@KDC.COM": 
Principal "testuser@KDC.COM" created.
kadmin:  ktadd -k /tmp/testuser.client.keytab nfs/nfsclient.kdc.com
Entry for principal nfs/nfsclient.kdc.com with kvno 3, encryption type aes256-cts-hmac-sha1-96 added to keytab WRFILE:/tmp/testuser.client.keytab.
Entry for principal nfs/nfsclient.kdc.com with kvno 3, encryption type aes128-cts-hmac-sha1-96 added to keytab WRFILE:/tmp/testuser.client.keytab.
Entry for principal nfs/nfsclient.kdc.com with kvno 3, encryption type arcfour-hmac added to keytab WRFILE:/tmp/testuser.client.keytab.
Entry for principal nfs/nfsclient.kdc.com with kvno 3, encryption type camellia256-cts-cmac added to keytab WRFILE:/tmp/testuser.client.keytab.
Entry for principal nfs/nfsclient.kdc.com with kvno 3, encryption type camellia128-cts-cmac added to keytab WRFILE:/tmp/testuser.client.keytab.
kadmin:  quit

[root@nfsclient /]# mkdir -p /var/kerberos/krb5/user/$(id -u testuser)/
[root@nfsclient /]# mv /tmp/testuser.client.keytab /var/kerberos/krb5/user/$(id -u testuser)/client.keytab
[root@nfsclient /]# chown testuser:testuser /var/kerberos/krb5/user/$(id -u testuser)/client.keytab
[root@nfsclient /]# ls -la /var/kerberos/krb5/user/$(id -u testuser)/client.keytab
-rw-------. 1 testuser testuser 394 Jun 17 06:04 /var/kerberos/krb5/user/1051/client.keytab

# Mount the filesystem
# I assume here that you've already set up
#   host/nfsserver.kdc.com
#   nfs/nfsserver.kdc.com
#
#   host/nfsclient.kdc.com
#   nfs/nfsclient.kdc.com
#
# copied the relevant configurations and default keytabs (either manually or via kadmin) to both server and client
# and started the necessary services.
mount nfsserver.kdc.com:/kerberos /mnt

# Test the user
[root@nfsclient /]# sudo -i -u testuser

# Authenticate with the default principal for the user (testuser@KDC.COM)
[testuser@nfsclient ~]$ kinit -V 
Using default cache: 1051
Using principal: testuser@KDC.COM
Password for testuser@KDC.COM: 
Authenticated to Kerberos v5

# Check that the default principal is actually testuser@KDC.COM
[testuser@nfsclient ~]$ klist
Ticket cache: KCM:1051
Default principal: testuser@KDC.COM

Valid starting       Expires              Service principal
06/17/2021 06:12:22  06/18/2021 06:12:14  krbtgt/KDC.COM@KDC.COM
        renew until 06/17/2021 06:12:22

# Check that the users client.keytab, containing key for nfs/nfsclient.kdc.com
# - clients NFS service principal - is being picked up
# (can access nfs share)
#
# and that the correct effective user is now used for file operation
# (derived from default principal and mapped to server-local user)
# - the server should also have a `testuser` user
#
[testuser@nfsclient /]$ touch /mnt/testfile
[testuser@nfsclient /]$ ls -la /mnt/testfile
-rw-rw-r--. 1 testuser testuser 0 Jun 17  2021 /mnt/testfile
```